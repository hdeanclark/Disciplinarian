

Introduction
============

  This project has a simple goal: organize and prioritize tasks. The intent is
to give the user some place to organize their tasks and goals, and also provide
a means of reporting what has yet to be done, what is most important, and what
is nearing a deadline.

  If successful, this project will result in a realtime priority scheduler. It
should handle, and successfully differentiate, self-imposed goals, external
tasks and deadlines, and events. At any moment, the user should be able to
query not only "What needs to be done?" but also "To maximize my time and goal
completion, what should I be doing right now?".


Installation
============

  1. This project uses CMake to build. Use the usual commands to compile:
     $> cd /path/to/source/directory
     $> mkdir build && cd build/
      THEN
       # If by-passing your package manager.
         $> cmake ../ -DCMAKE_INSTALL_PREFIX=/usr
         $> make && sudo make install
      OR
       # If building for Debian.
         $> cmake ../ -DCMAKE_INSTALL_PREFIX=/usr
         $> make && make package 
         $> sudo apt-get install -f ./*.deb  # Or [--fix-missing] in lieu of [-f].
      OR 
       # If building for Arch Linux.
         $> rsync -a --exclude build ../ ./
         $> makepkg --syncdeps --noconfirm # Optionally also [--install].


  2. Create the task store:
     $> mkdir -p ~/Disciplinarian_Tasks/{Active,Inactive}/
     $> cd ~/Disciplinarian_Tasks/
     $> git init

     NOTE: A store will be created automatically when the first task is created,
     but it will not be git-enabled.


  3. Tweak the global config file [/etc/disciplinarian.conf] or copy it to your
     home directory [~/.disciplinarian.conf] and edit for per-user configuration.


  4. (Optional) Configure vim to jump cross-linked tasks. Add the contents of 
     [other/vimrc_snippet.txt] to your [~/.vimrc].

     This will enable you to press a shortcut and open another vim pane with the
     linked task UID. It makes navigating cross-linked tasks easier, but is not
     needed.

How-To
======

  - After installation, you can create tasks via
      $> create_deadline
    which will interactively build a task and add it to the store. 

  - Tasks in the store can be interactively queried via
      $> what_to_do -t 0 
    There are several operations possible. For information on additional options
    invoke:
      $> what_to_do -h

    Generally, you can record notes, track time spent on tasks, and prioritize 
    and manage tasks via `what_to_do`.

  - More in-depth analysis can be performed by invoking:
      $> analyze_tasks

    This program can send HTTP POST emails, which is handy if you have access to
    an email-as-a-service service. Private authentication details and email 
    addresses can be passed as commandline arguments, stored in 
    [~/.disciplinarian.conf], or stored (globally) in [/etc/disciplinarian.conf].

    To launch a daily summary of Disciplinarian tasks, a systemd timer can be 
    used like so:
      $> systemd-run --user --no-block \
           --unit=daily_disciplinarian_task_analysis.service \
           --description="Daily Disciplinarian task analysis" \
           --on-calendar='*-*-* 03:30:23'    analyze_tasks -u -s

    A summary email will be sent every day at 3:30 am. The timer/service status
    can be inspected via:
      $> systemctl status --user daily_disciplinarian_task_analysis.timer



Approach
========

  The user specifies some basic information about tasks: a title and optional
description, a deadline (maybe ongoing, maybe recurring), and some optional
info: a current priority score, a final priority score, and a priority
`schedule' between the date of specification and the deadline. The schedule can
be any function, f(t), which can be used to determine which task is currently
the most important.


Notes
=====

  Lines beginning with (any amount of) whitespace followed by a '#' are 
considered comments and will be ignored by the parser. If a file is edited
then the comments will NOT be written. Such comments are thus only useful for
archived task files. This is more-or-less a bug, and may be addressed in the
future.


Limitations
===========

  Plenty of limitations exist. This is a slow-moving WIP project.

  The current file store implementation assumes a plain git repository. This 
provides version history out-of-the-box, but requires tasks to be stored as
text files in a directory hierarchy. File discovery and re-parsing can be slow.
You can speed up task enumeration and parsing by partitioning task files into
'active' and 'inactive' groups and minimizing parsing the 'inactive' group
files.

  Cross-linking tasks is possible using UIDs, but the mechanism is hacky and 
currently vim-specific.


Author and License
==================

  All code, unless otherwise indicated, was written by hal clark over
2013-2017. It is released under the GPLv3. This project was undertaken as a
personal hobby project.

