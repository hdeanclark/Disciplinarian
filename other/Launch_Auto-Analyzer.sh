#!/usr/bin/env bash

#
# This script invokes an ephemeral systemd unit file that reports an overview of the
# current state of the user's tasks. It triggers in early morning and attempts to send
# an email using credentials from the user's config file(s).
#
# Written by Hal Clark in 2017.
#

systemd-run --user --no-block \
  --unit=daily_disciplinarian_task_analysis.service \
  --description="Daily Disciplinarian task analysis"  \
  --on-calendar='*-*-* 03:30:23'  \
  analyze_tasks -u -s

