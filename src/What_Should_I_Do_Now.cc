//What_Should_I_Do_Now.cc - The core read-only interface to Project - Disciplinarian.
// This program checks the database of deadlines and events and notifies the user
// which task they should be working on at the moment.
//

#include <cstdint> //For intmax_t

#include <list>
#include <string>
#include <memory>
#include <vector>
#include <iomanip>
#include <functional>
#include <unistd.h>    //Needed for execve().
#include <sstream>
#include <algorithm>

#include <boost/program_options.hpp>

#include "YgorMisc.h"
#include "YgorAlgorithms.h"
#include "YgorEnvironment.h"
#include "YgorSerialize.h"
#include "YgorString.h"
#include "YgorArguments.h"

#include "Structs.h"

#include "Helpers.h"

uint64_t Total_Est_Length(const std::list<Deadline> &in){
    uint64_t out = 0;
    for(auto it=in.begin(); it!=in.end(); ++it) out += it->Estimated_Length;
    return out;
}

double Total_Priority(const std::list<Deadline> &in){
    double out = 0.0;
    for(auto it=in.begin(); it!=in.end(); ++it) out += it->Get_Current_Priority();
    return out;
}

double Total_Work_Done_On_Day(const std::list<Deadline> &in, const time_mark &tday){
    double out = 0.0;
    for(auto it=in.begin(); it!=in.end(); ++it) out += it->Get_Total_Working_Time_On_Day(tday);
    return out;
}

int main(int argc, char **argv){

    //Environment variables.
    const uint32_t override_usage_term_width = 160;
    const uint32_t override_usage_reserve_width = 50;

    const std::string progname(argv[0]);

    const std::vector<std::string> Possible_Config_Files {   
        //Highest-preference files first.
        (Get_Users_Home_Directory() + "/.disciplinarian.conf"),
        "/etc/disciplinarian.conf" 
    };

    //Program variables and behavioural switches.
    std::list<Deadline> Tasks;

    std::string Base_Dir;

    double hidey_threshold = 0.0;    //Hide tasks with a threshold lower than this value.
    size_t long_desc_count = 0;      //Show long descriptions for this many tasks only.
    bool Show_Completed = false;     //Show completed tasks. Showing not recommended except for inspection purposes.
    bool Show_Abandoned = false;     //Show abandoned tasks. Showing not recommended except for inspection purposes.
    size_t Desc_Line_Limit = 3;      //Only show this many lines of descriptions. 0 implies 'show all.' Useful for quick overviews.  
    bool Dump_Stats_Only = false;    //Compute & dump some stats and then exit.   
    std::vector<std::string> Grep_Substr;     //Substrings for case-insensitive searching of titles, descriptions, notes, tags, and UIDs. Ignores thresholds.
    std::vector<std::string> Tag_Grep_Substr; //Substrings for case-insensitive searching of tags only. Ignores thresholds.
    bool Sort_Older_First = false;   //Display tasks which end oldest first. Helps when pruning old tasks.
    bool Sort_Short_First = false;   //Display tasks which are estimated to require the least time first. Good for getting started.
    size_t Stats_Number_of_Days = 7; //The number of days of stats to print out.


    // Argument handling and config file parsing.
    {
        namespace po = boost::program_options;

        po::options_description RegularOptions("General Options",
                                               override_usage_term_width,
                                               override_usage_reserve_width);
        RegularOptions.add_options()
           ( "help,h",
             "  Display usage.\n" )
           ( "threshold,t", 
             po::value<double>(&hidey_threshold)->default_value(hidey_threshold),
             "  Do not show tasks with priority below this value.\n"
             "  \tExamples:\n"
             "  \t  0.05\n"
             "  \t  0.8\n"
             "  \t  1.0\n" )
           ( "num-long-descriptions,n", 
             po::value<size_t>(&long_desc_count)->default_value(long_desc_count), 
             "  Show long descriptions for this many (top priority) tasks.\n"
             "  \tExamples:\n"
             "  \t  0\n"
             "  \t  10\n"
             "  \t  1000\n" )
           ( "show-complete,c",
             po::bool_switch(&Show_Completed)->default_value(false),
             "  Show completed tasks. (You'll probably want a threshold of 0.0 too.)\n" )
           ( "show-abandoned,a",
             po::bool_switch(&Show_Abandoned)->default_value(false),
             "  Show abandoned tasks. (You'll probably want a threshold of 0.0 too.)\n" )
           ( "max-desc-lines,l", 
             po::value<size_t>(&Desc_Line_Limit)->default_value(Desc_Line_Limit), 
             "  How many lines of descriptions are shown.\n"
             "  \tExamples:\n"
             "  \t  0\n"
             "  \t  10\n"
             "  \t  1000\n" )
           ( "grep,g",
             po::value< std::vector<std::string> >(&Grep_Substr),
             "  A substring to perform case-insensitive searching in title, descriptions, notes, tags, and UIDs.\n"
             "  \tExamples:\n"
             "  \t  'important'\n"
             "  \t  'some substring'\n" )
           ( "tag-grep,G",
             po::value< std::vector<std::string> >(&Tag_Grep_Substr),
             "  A substring to perform case-insensitive searching in tags only.\n"
             "  \tExamples:\n"
             "  \t  'important'\n"
             "  \t  'some substring'\n" )
           ( "older-first,o",
             po::bool_switch(&Sort_Older_First)->default_value(false),
             "  Order tasks on end date. Older first. Useful for pruning old tasks.\n" )
           ( "shortest-first,e",
             po::bool_switch(&Sort_Short_First)->default_value(false),
             "  Order tasks on estimated time. Shortest first. Useful for getting started with quick tasks.\n" )
           ( "dump-stats,s",
             po::bool_switch(&Dump_Stats_Only)->default_value(false),
             "  Dump stats regarding the specified tasks and immediately exit.\n" )
           ( "stats-days,d",
             po::value<size_t>(&Stats_Number_of_Days)->default_value(Stats_Number_of_Days),
             "  Number of days in the past back to dump stats for (inclusive).\n"
             "  \tExamples:\n"
             "  \t  1\n"
             "  \t  10\n"
             "  \t  1000\n" )
           ( "base-dir,B",
             po::value< std::string >(&Base_Dir),
             "  The path to the root directory of the task store."
              " Note that Bash syntax '~/' is supported as long as the 'HOME' environment variable is defined.\n"
             "  \tExamples:\n"
             "  \t  '~/disciplinarian_store/'\n"
             "  \t  '~/disciplinarian_store/'\n"
             "  \t  '/home/someuser/disciplinarian_store/'\n"
             "  \t  '~/.task_store/'\n" )
        ;

        po::positional_options_description PositionalOptions;
        PositionalOptions.add("grep", -1); //Assume all un-marked options are greps ... needed ??? TODO ???.

        // Parse command line options.
        po::variables_map VarMap;
        po::store(
            po::command_line_parser(argc, argv)
              .options(RegularOptions)
              .positional(PositionalOptions)
              .allow_unregistered()
              .run(),
            VarMap);

        // Parse any readable config files.
        for(const auto &f : Possible_Config_Files){
            std::ifstream fif(f);
            if(fif){
                const bool IgnoreUnregistered = true;
                po::store(po::parse_config_file(fif, RegularOptions, IgnoreUnregistered), VarMap);
            }
        }

        po::notify(VarMap);

        //Print usage.
        if(VarMap.count("help")){
            // ... = VarMap["show-complete"].as<bool>();
            std::cout << progname << " -- A part of Disciplinarian." << std::endl;
            std::cout << RegularOptions << std::endl;
            return 0;
        }

    }

    //Argument post-processing.
    const bool Hide_Completed = not Show_Completed;
    const bool Hide_Abandoned = not Show_Abandoned;

    if(Base_Dir.empty()){
        throw std::invalid_argument("No base directory provided. Cannot continue.");
    }
    Base_Dir = Expand_Tilde_Home_Directory(Base_Dir);

    ////////////////////////////////////////////////////////////////////////////////////////////////

    //Find all tasks in the store. Try to read them all.
    //
    //Note: Currently, all text-file tasks live in their own file. 
    const auto Active_Dir   = Remove_Trailing_Chars(Base_Dir,"/") + "/Active/";
    const auto Inactive_Dir = Remove_Trailing_Chars(Base_Dir,"/") + "/Inactive/";
    auto Filenames = Get_List_of_Full_Path_File_Names_in_Dir(Active_Dir);
    if(!Hide_Completed || !Hide_Abandoned){
        if(!Append_List_of_Full_Path_File_Names_in_Dir(Filenames,Inactive_Dir)){
            FUNCERR("Unable to access '" << Inactive_Dir << "'. Not continuing");
        }
    }
    Filenames.sort();
    std::unique(std::begin(Filenames), std::end(Filenames));

    for(auto l_it = Filenames.begin(); l_it != Filenames.end(); ++l_it){
        //Attempt to parse it.
        Tasks.push_back(Deadline());

        const auto suffix = Get_Trailing_Chars(*l_it, 4);
        if(".TXT" != Canonicalize_String2(suffix,CANONICALIZE::TO_UPPER)){ //Ignore vim swap files.
            FUNCWARN("Ignoring file '" << *l_it << "'");
            Tasks.pop_back();
        }else if(!(Tasks.back().Read_From_File(*l_it))){ //Ignore if it cannot be read or parsed properly.
            Tasks.pop_back();
        }else if(Hide_Completed && (Tasks.back().Status == dspln::status::completed)){  //Ignore completed tasks...
            Tasks.pop_back();
        }else if(Hide_Abandoned && (Tasks.back().Status == dspln::status::abandoned)){  //Ignore abandoned tasks...
            Tasks.pop_back();
        }
    }




    //Remove those tasks in which the search sub-string is not found.
    if(!Grep_Substr.empty()){
        //Get an all-capitalized version of the search sub-strings.
        std::vector<std::string> substr_incaps;
        for(const auto & substr : Grep_Substr) substr_incaps.push_back( Canonicalize_String2(substr,CANONICALIZE::TO_UPPER) );

        auto task_it = Tasks.begin();
        while(task_it != Tasks.end()){
            
            const auto title_incaps = Canonicalize_String2(task_it->Title,CANONICALIZE::TO_UPPER);
            const auto desc_incaps  = Canonicalize_String2(task_it->Description,CANONICALIZE::TO_UPPER);
            const auto notes_incaps = Canonicalize_String2(task_it->Notes,CANONICALIZE::TO_UPPER);
            const auto tags_incaps  = Canonicalize_String2(task_it->Tags,CANONICALIZE::TO_UPPER);
            const auto uid_incaps   = Canonicalize_String2(task_it->UID,CANONICALIZE::TO_UPPER);
            const auto concat = title_incaps + "\n" + desc_incaps 
                                             + "\n" + notes_incaps 
                                             + "\n" + tags_incaps
                                             + "\n" + uid_incaps;

            bool are_all_present = true;
            for(const auto & substr : substr_incaps){
                are_all_present = (are_all_present) && (concat.find(substr) != std::string::npos);
            }

            if( are_all_present ){
                ++task_it; //Then continue on to the next.
            }else{
                task_it = Tasks.erase(task_it); //Otherwise, remove it. We aren't interested.
            }
        }
    }

    //Remove those tasks in which the tag search sub-string is not found.
    if(!Tag_Grep_Substr.empty()){
        //Get an all-capitalized version of the search sub-strings.
        std::vector<std::string> substr_incaps;
        for(const auto & substr : Tag_Grep_Substr) substr_incaps.push_back( Canonicalize_String2(substr,CANONICALIZE::TO_UPPER) );

        auto task_it = Tasks.begin();
        while(task_it != Tasks.end()){
            
            const auto tags_incaps  = Canonicalize_String2(task_it->Tags,CANONICALIZE::TO_UPPER);

            bool are_all_present = true;
            for(const auto & substr : substr_incaps){
                are_all_present = (are_all_present) && (tags_incaps.find(substr) != std::string::npos);
            }

            if( are_all_present ){
                ++task_it; //Then continue on to the next.
            }else{
                task_it = Tasks.erase(task_it); //Otherwise, remove it. We aren't interested.
            }
        }
    }



    if(Tasks.empty()){
        FUNCWARN("No available tasks. Ensure this is not a mistake");
        return 0;
    }

    //Sort by priority. This is the default.
    Tasks.sort(Deadline_Sort_Compare_Higher_Priority_First);

    //Sort by deadline, older end times first.
    if(Sort_Older_First) Tasks.sort(Deadline_Sort_Compare_Older_First);
    
    //Sort by estimated time, shorter times first.
    if(Sort_Short_First) Tasks.sort(Deadline_Sort_Compare_Short_First);
    


    //Stats dumping.
    if(Dump_Stats_Only){
        const auto N = Tasks.size();

        std::cout << "Number of tasks considered:             ";
        std::cout << N << std::endl;

        const auto TotEstLen = Total_Est_Length(Tasks);
        std::cout << "Est. length of all tasks:               ";
        std::cout << TotEstLen << " min, or " << TotEstLen/60 << " hours, or ";
        std::cout << TotEstLen/60/8 << " 8-hour-days" << std::endl;

        std::cout << "Avg est. length of all tasks:           ";
        std::cout << TotEstLen/N << " min, or " << TotEstLen/N/60 << " hours";
        std::cout << std::endl;

        const auto TotPri = Total_Priority(Tasks);
        std::cout << "Total priority of tasks:                ";
        std::cout << TotPri << std::endl;

        std::cout << "Avg. priority of tasks:                 ";
        std::cout << TotPri/N << std::endl;

        time_mark tspec;
        for(size_t i=0; i<=Stats_Number_of_Days; ++i){
            //Spit out the day's working times.
            const auto TotWorkYest = Total_Work_Done_On_Day(Tasks,tspec);
            std::cout << "Total (indep.) Work done " << std::setw(2) << i << " days back:   ";
            std::cout << std::fixed << std::setw(5) << std::setprecision(0);
            std::cout << TotWorkYest << " min, or ";
            std::cout << std::fixed << std::setw(5) << std::setprecision(1);
            std::cout << TotWorkYest/60.0 << " hours";
            std::cout << std::endl;

            //Spit out the specific tasks worked on.
            for(auto it=Tasks.begin(); it!=Tasks.end(); ++it){
                const auto dt = it->Get_Total_Working_Time_On_Day(tspec);
                if(dt <= 0.0) continue;
                std::cout << "        ";
                std::cout << std::fixed << std::setw(5) << std::setprecision(0) << dt;
                std::cout << " " << PrintYellow(it->Title) << std::endl;
            }

            tspec.Regress_One_Day();
        }

        // ... put anything of interest here ...

        return 0;
    }


    //Determine the terminal width, for "pretty-printing purposes"
    const auto term_dims = Get_Terminal_Char_Dimensions();
    const auto term_w = (term_dims.first > 0) ? term_dims.first : 80;
    const auto term_h = (term_dims.second > 0) ? term_dims.second : 24;
    //FUNCINFO("Terminal dimensions: wxh = " << term_w << "x" << term_h);

    const auto term_lw = static_cast<long int>(0.6f*static_cast<float>(term_w));

    size_t num = 0;
    for(auto l_it = Tasks.begin(); l_it != Tasks.end(); ++l_it, ++num){

        //Skip showing totally insignificant tasks unless they are currently being worked on. 
        // TODO: "sliding window" of what is considered insignificant!
        if((l_it->Get_Current_Priority() < hidey_threshold) && !l_it->Is_Curr_Worked_On()) continue;

        //Print the task number, priority, title, end date, and filename.
        std::cout << std::setw(2) << num;
        std::cout << " - " << std::fixed << std::setprecision(5) << l_it->Get_Current_Priority();
        std::cout << " - " << std::fixed << std::setw(5) << l_it->Estimated_Length;
        std::cout << " - " << std::fixed << std::setw(5) << std::setprecision(0) << l_it->Get_Current_Work_Time();
        std::cout << " - " << l_it->End.Dump_as_string();
        std::cout << " - " << PrintYellow(l_it->Title);
        std::cout << " - " << Get_Bare_Filename(l_it->Filename_Origin);
        std::cout << std::endl;

        //Show a big notice if the task is currently being worked on.
        if(!(l_it->Working.empty())){
            if(l_it->Is_Curr_Worked_On()){
                std::cout << PrintRed("      ==== Currently being worked on ====") << std::endl;
            }
        }

        //Print descriptions and notes (only for the top few).
        if(num < long_desc_count){
            const long int desc_w = (l_it->Notes.empty()) ? term_w - 8 : term_lw - 6;
            const long int note_w = (l_it->Notes.empty()) ? 2          : (term_w - term_lw) - 2;
            //FUNCINFO("Item #" << num << ": desc_w=" << desc_w << " and note_w=" << note_w);

            auto DescNotes = Reflow_Adjacent_Texts_to_Fit_Width_Left_Just(l_it->Description,desc_w,0,
                                                                     "  ",l_it->Notes,note_w,0);
            size_t line_cnt = 0;
            for(auto dnl_it = DescNotes.begin(); dnl_it != DescNotes.end(); ++dnl_it, ++line_cnt){
                if((Desc_Line_Limit != 0) && (Desc_Line_Limit == line_cnt)) break; //Line limit reached.
                std::cout << "    " << *dnl_it << std::endl;
            }
            std::cout << std::endl;
        }
    }


    std::cout << "It is currently " << PrintYellow(time_mark().Dump_as_string()) << ". What do you want to do?" << std::endl;

    std::cout << "  mark task [" << PrintYellow("C") << "]ompleted;               ";  // 1
    std::cout << "[" << PrintYellow("B") << "]egin work on a task;              "; // 2
    std::cout << "[" << PrintYellow("E") << "]nd work on a task;    " << std::endl; // 3

    std::cout << "  began work N minutes [" << PrintYellow("A") << "]go;          "; // 1
    std::cout << "[" << PrintYellow("I") << "]nsert a block of work;            "; // 2
    std::cout << "[" << PrintYellow("O") << "]pen task with vim;    " << std::endl; // 3

    std::cout << "  [" << PrintYellow("S") << "]tart a task (begin and open);     "; // 1
    std::cout << "[" << PrintYellow("F") << "]inish a task (end and complete);  "; // 2
    std::cout << "began in [" << PrintYellow("P") << "]ast (N ago and open);" << std::endl; // 3

    std::cout << "  [" << PrintYellow("T") << "]idy in-active tasks.              "; // 1
    std::cout << "Anything else to [" << PrintYellow("Q") << "]uit. " << std::endl;

    // ... maybe abandon a task?

    std::string resp;
    while(true){
        resp = Poll_Until_We_Get_A_<std::string>("Your choice...");
        if( (resp == "c") || (resp == "C")
        ||  (resp == "b") || (resp == "B")
        ||  (resp == "e") || (resp == "E") 
        ||  (resp == "f") || (resp == "F") 
        ||  (resp == "s") || (resp == "S")
        ||  (resp == "a") || (resp == "A") 
        ||  (resp == "p") || (resp == "P")
        ||  (resp == "i") || (resp == "I")
        ||  (resp == "t") || (resp == "T")
        ||  (resp == "o") || (resp == "O") ){
            break;
        }else{
            FUNCINFO("Goodbye");
            return 0;
        }
    }

    char *basedir = const_cast<char *>(Base_Dir.c_str());

    // ---- Operations without a specified file ----
    //Tidy the store by partitioning inactive tasks into separate directory.
    if((resp == "t") || (resp == "T")){
        char theeditor[] = "disciplinarian_tidy_store.sh"; //execlp will perform path resolution for us.
        if(-1 == execlp(theeditor,theeditor,basedir,(char *)nullptr)){
            FUNCERR("Couldn't execve disciplinarian_tidy_store.sh");
        }//Does not return on success - overwrites the stack.
    }


    // ---- Operations with a specified file ----
    //Figure out which task the user wants to modify.
    while(true){
        num = Poll_Until_We_Get_A_<size_t>("Which deadline/task number?");
        if(num < Tasks.size()) break;
    }
    auto thetask = std::next(Tasks.begin(),num);
    char *thefilename = const_cast<char *>(thetask->Filename_Origin.c_str()); // const casts OK - we are either exec'ing or dying.

    //Mark a task as completed.
    if((resp == "c") || (resp == "C")){
        if(thetask->Is_Curr_Worked_On()){
            FUNCERR("Task is being worked on. Mark work as completed first. Refusing to mark");
        }
        thetask->Status = dspln::status::completed;
        thetask->Completed = time_mark();
        if(!Overwrite_Updated_Task(*thetask)) FUNCERR("Could not mark task as completed");

        char theeditor[] = "disciplinarian_autocommit_completed.sh"; //execlp will perform path resolution for us.
        if(-1 == execlp(theeditor,theeditor,thefilename,basedir,(char *)nullptr)){
            FUNCERR("Couldn't execve disciplinarian_autocommit_completed.sh. The filename is '" << thetask->Filename_Origin << "'");
        }//Does not return on success - overwrites the stack.


    //Begin work on a task. 
    }else if((resp == "b") || (resp == "B")){
        if(thetask->Is_Curr_Worked_On()){
            FUNCERR("Task is already being worked on. Refusing to mark");
        }
        thetask->Mark_Work_Began();
        if(!Overwrite_Updated_Task(*thetask)) FUNCERR("Could not mark work begin");

        char theeditor[] = "disciplinarian_autocommit_began.sh"; //execlp will perform path resolution for us.
        if(-1 == execlp(theeditor,theeditor,thefilename,basedir,(char *)nullptr)){
            FUNCERR("Couldn't execve disciplinarian_autocommit_began.sh. The filename is '" << thetask->Filename_Origin << "'");
        }//Does not return on success - overwrites the stack.

    //End *work* on a task. 
    }else if((resp == "e") || (resp == "E")){
        if(!thetask->Is_Curr_Worked_On()){
            FUNCERR("Task is not being worked on. Refusing to mark");
        }
        thetask->Mark_Work_Ended();
        if(!Overwrite_Updated_Task(*thetask)) FUNCERR("Could not mark work end");

        char theeditor[] = "disciplinarian_autocommit_end.sh"; //execlp will perform path resolution for us.
        if(-1 == execlp(theeditor,theeditor,thefilename,basedir,(char *)nullptr)){
            FUNCERR("Couldn't execve disciplinarian_autocommit_end.sh. The filename is '" << thetask->Filename_Origin << "'");
        }//Does not return on success - overwrites the stack.

    //"Finish" a task; end work and mark it as completed. 
    }else if((resp == "f") || (resp == "F")){
        //Check to ensure the task is currently being worked on.
        if(!thetask->Is_Curr_Worked_On()){
            FUNCERR("Task is not being worked on. Refusing to mark");
        }
        thetask->Mark_Work_Ended();
        thetask->Status = dspln::status::completed;
        thetask->Completed = time_mark();
        if(!Overwrite_Updated_Task(*thetask)) FUNCERR("Could not 'finish' the task");

        char theeditor[] = "disciplinarian_autocommit_finish.sh"; //execlp will perform path resolution for us.
        if(-1 == execlp(theeditor,theeditor,thefilename,basedir,(char *)nullptr)){
            FUNCERR("Couldn't execve disciplinarian_autocommit_finish.sh. The filename is '" << thetask->Filename_Origin << "'");
        }//Does not return on success - overwrites the stack.

    //"Start" a task; mark it as begun AND open it for editing.
    }else if((resp == "s") || (resp == "S")){
        if(thetask->Is_Curr_Worked_On()){
            FUNCERR("Task is already being worked on. Refusing to mark");
        }
        thetask->Mark_Work_Began();
        if(!Overwrite_Updated_Task(*thetask)) FUNCERR("Could not mark work begin");

        FUNCINFO("execve-ing to 'disciplinarian_start_edit_commit.sh'. Goodbye");

        char theeditor[] = "disciplinarian_start_edit_commit.sh"; //execlp will perform path resolution for us.
        if(-1 == execlp(theeditor,theeditor,thefilename,basedir,(char *)nullptr)){
            FUNCERR("Couldn't execve disciplinarian_start_edit_commit.sh. The filename is '" << thetask->Filename_Origin << "'");
        }//Does not return on success - overwrites the stack.

    //Try mark the last N minutes as Working time.
    }else if((resp == "a") || (resp == "A")){
        const auto N = Poll_Until_We_Get_A_<int64_t>("How many minutes ago did you start?");
        const time_mark now;
        const time_mark then = time_mark().Less_By_Seconds(N*60);

        //Check if any timestamps lie within then and now.
        auto Conflicting_Working_Timestamps = thetask->Working_Between_Inc(now,then);
        if(!Conflicting_Working_Timestamps.empty()){
            FUNCERR("Existing Working timestamps conflict with specified N");

        //Check if then and now are bounded by an existing Working block.
        }else if(thetask->Was_Being_Worked_On_Between_Inc(now,then)){
            FUNCERR("Specified time is already working time"); 
        }

        thetask->Mark_Work_Began_At(then);
        if(!Overwrite_Updated_Task(*thetask)) FUNCERR("Could not mark work began");

        char theeditor[] = "disciplinarian_autocommit_past.sh"; //execlp will perform path resolution for us.
        if(-1 == execlp(theeditor,theeditor,thefilename,basedir,(char *)nullptr)){
            FUNCERR("Couldn't execve disciplinarian_autocommit_past.sh. The filename is '" << thetask->Filename_Origin << "'");
        }//Does not return on success - overwrites the stack.

    //Try mark the last N minutes as Working time AND open it for editing.
    }else if((resp == "p") || (resp == "P")){
        const auto N = Poll_Until_We_Get_A_<int64_t>("How many minutes ago did you start?");
        const time_mark now;
        const time_mark then = time_mark().Less_By_Seconds(N*60);

        //Check if any timestamps lie within then and now.
        auto Conflicting_Working_Timestamps = thetask->Working_Between_Inc(now,then);
        if(!Conflicting_Working_Timestamps.empty()){
            FUNCERR("Existing Working timestamps conflict with specified N");

        //Check if then and now are bounded by an existing Working block.
        }else if(thetask->Was_Being_Worked_On_Between_Inc(now,then)){
            FUNCERR("Specified time is already working time");
        }

        thetask->Mark_Work_Began_At(then);
        if(!Overwrite_Updated_Task(*thetask)) FUNCERR("Could not mark work began");

        FUNCINFO("execve-ing to 'disciplinarian_past_edit_commit.sh'. Goodbye");

        char theeditor[] = "disciplinarian_past_edit_commit.sh"; //execlp will perform path resolution for us.
        if(-1 == execlp(theeditor,theeditor,thefilename,basedir,(char *)nullptr)){
            FUNCERR("Couldn't execve disciplinarian_past_edit_commit.sh. The filename is '" << thetask->Filename_Origin << "'");
        }//Does not return on success - overwrites the stack.

    //Try to insert a block of Working time.
    }else if((resp == "i") || (resp == "I")){
        const auto tA = Poll_Until_We_Get_A_<time_mark>("When did the work begin? Use format: '" + time_mark().Dump_as_string() + "'");
        const auto tB = Poll_Until_We_Get_A_<time_mark>("When did the work end?   Use format: '" + time_mark().Dump_as_string() + "'");

        if(tA >= tB){
            FUNCERR("Times are nonsensical. Refusing to write to task");
        }

        //Check if any timestamps lie within then and now.
        auto Conflicting_Working_Timestamps = thetask->Working_Between_Inc(tA,tB);
        if(!Conflicting_Working_Timestamps.empty()){
            FUNCERR("Existing Working timestamps conflict with specified N");

        //Check if then and now are bounded by an existing Working block.
        }else if(thetask->Was_Being_Worked_On_Between_Inc(tA,tB)){
            FUNCERR("Specified time is already working time");
        }

        thetask->Mark_Work_Began_At(tA);
        thetask->Mark_Work_Ended_At(tB);
        if(!Overwrite_Updated_Task(*thetask)) FUNCERR("Could not mark work block");

        char theeditor[] = "disciplinarian_autocommit_insert.sh"; //execlp will perform path resolution for us.
        if(-1 == execlp(theeditor,theeditor,thefilename,basedir,(char *)nullptr)){
            FUNCERR("Couldn't execve disciplinarian_autocommit_insert.sh. The filename is '" << thetask->Filename_Origin << "'");
        }//Does not return on success - overwrites the stack.

    //Edit the task file directly using vim.
    }else if((resp == "o") || (resp == "O")){
        FUNCINFO("execve-ing to 'disciplinarian_open_edit_commit.sh'. Goodbye");

        char theeditor[] = "disciplinarian_open_edit_commit.sh"; //execlp will perform path resolution for us.
        if(-1 == execlp(theeditor,theeditor,thefilename,basedir,(char *)nullptr)){
            FUNCERR("Couldn't execve disciplinarian_open_edit_commit.sh. The filename is '" << thetask->Filename_Origin << "'");
        }//Does not return on success - overwrites the stack.

    }

    return 0;
}
