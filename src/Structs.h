//Structs.h

#ifndef PROJECT_DISCIPLINARIAN_STRUCTS_H_HDR_GRD
#define PROJECT_DISCIPLINARIAN_STRUCTS_H_HDR_GRD 

#include <list>
#include <string>
#include <memory>
//#include <utility>
#include <map>

#include "YgorMisc.h"
#include "YgorTime.h"


namespace dspln {
    namespace status {
        const uint64_t active     = 1;  //This is fixed. Do not change!
        const uint64_t completed  = 2;  //This is fixed. Do not change!
        const uint64_t abandoned  = 3;
    }

    namespace sched {
        const uint64_t linear               = 10;

        //Sigmoidal schedules.
        const uint64_t sigmoidal            = 20;

        //End-minus-N-time schedules.
        const uint64_t gaussian_em1day      = 30;
        const uint64_t gaussian_em3days     = 31;
        const uint64_t gaussian_em1week     = 32;
        const uint64_t gaussian_em2weeks    = 33;
        const uint64_t gaussian_em1mon      = 34;
        const uint64_t gaussian_em2mons     = 35;


        //Estimated-length schedules.
        const uint64_t gaussian_em2xestlen   = 40;
        const uint64_t gaussian_em20xestlen  = 41;
        const uint64_t gaussian_em200xestlen = 42;
    }

    namespace working {
        const uint64_t began     = 1;
        const uint64_t ended     = 2;
    }
}


class Deadline {
    public:
        uint64_t Status;  
        uint64_t Schedule;   //Importance schedule. Used to compute importance at time t.
        uint64_t Estimated_Length; //User-estimated number of minutes req'd for task.

        double A, B, C, D;   //Schedule parameters. they have contextual meaning.
        double Ii, If;       //Scheduling: initial and final importance: [0.0:1.0].
                             // Note: If can be lower than Ii (i.e., decreasing importance).

        time_mark Begin;
        time_mark End;
        time_mark Completed;

        std::map<time_mark,uint64_t> Working; //Keeps track of when I began/stopped working on it.

        std::string UID;           //A unique, static ID for the task. Assigned once at creation.
        std::string Title;
        std::string Tags;          //A list of tags used for easier classification of tasks.
        std::string Description;
        std::string Notes;         //Human-readable freeform notes. 

        //Non-serialized members. (These only make sense during runtime.)
        std::string Filename_Origin;  //File from which *this originated.
        

        //Constructor, Destructor.
        Deadline();
        //~Deadline();

        //Member functions.
        void Populate_Reasonable_Defaults();

        double Get_Priority_At(const time_mark &) const;
        double Get_Current_Priority() const;  //Evaluates the current importance.

        bool Was_Being_Worked_On_Between_Inc(const time_mark &, const time_mark &) const;
        bool Was_Being_Worked_On_At(const time_mark &) const;
        bool Is_Curr_Worked_On() const;

        std::map<time_mark,uint64_t> Working_Between_Inc(const time_mark &, const time_mark &) const;


        double Get_Work_Time_At(const time_mark &) const; //Sum of timespans between 'work began' and 'work ended'.
        double Get_Current_Work_Time() const;

        double Get_Total_Working_Time_Between(const time_mark &, const time_mark &) const;
        double Get_Total_Working_Time_On_Day(const time_mark &) const;


        void Mark_Active();    //(Re-)opens the task.
        void Mark_Completed(); //Marks complete with completion time now.
      
        void Mark_Work_Began_At(const time_mark &);
        void Mark_Work_Ended_At(const time_mark &);
        void Mark_Work_Began();
        void Mark_Work_Ended();
 
        bool Verify_Consistency() const;


        bool Read_From_File(const std::string &filenamein);
        bool Write_To_File(const std::string &filenameout) const;
 
        //Serialize (deeply) to buffer starting at *offset. See source for more info.
        std::unique_ptr<uint8_t[]> Serialize(bool *OK, std::unique_ptr<uint8_t[]> in, uint64_t *offset, uint64_t *buf_size) const;
        //Deserialize (deeply) from buffer starting at *offset. See source for more info.
        std::unique_ptr<uint8_t[]> Deserialize(bool *OK, std::unique_ptr<uint8_t[]> in, uint64_t *offset, uint64_t buf_size);
        //Serialization helpers.
        uint64_t Theo_Max_Serialization_Size(void) const;
};


bool Deadline_Sort_Compare_Higher_Priority_First(const Deadline &A, const Deadline &B);

bool Deadline_Sort_Compare_Older_First(const Deadline &A, const Deadline &B);

bool Deadline_Sort_Compare_Short_First(const Deadline &A, const Deadline &B);

bool Deadline_Sort_Compare_Highest_Importance_Derivative_First(const Deadline &A, const Deadline &B);

bool Deadline_Sort_Compare_Closest_To_Deadline_First(const Deadline &A, const Deadline &B);

bool Deadline_Sort_Compare_Most_Recently_Worked_On_First(const Deadline &A, const Deadline &B);

#endif //PROJECT_DISCIPLINARIAN_STRUCTS_H_HDR_GRD
