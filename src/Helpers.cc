//Helpers.cc - Useful functions which should be made available to all programs in 
// this project, but which do not quite deserve to be class members or (not yet) 
// in Ygor.
//
//Routines which require interaction from the user are a good fit here.

#include <cstdint> //For intmax_t

#include <list>
#include <string>
#include <memory>
#include <vector>
#include <iomanip>
#include <functional>

#include <unistd.h>    //Needed for execve().
#include <sys/types.h>
#include <pwd.h>

#include "YgorMisc.h"
#include "YgorAlgorithms.h"
#include "YgorEnvironment.h"
#include "YgorSerialize.h"
#include "YgorString.h"
#include "YgorArguments.h"

#include "Structs.h"

#include "Helpers.h"

//This was borrowed from Project - Calories. There is a more sophisticated
// version lurking there, so use it if something more is required...
template <class T> T Poll_Until_We_Get_A_(const std::string& msg){
    while(true){
        std::string in;
        std::cout << msg << std::endl;
        std::cout.flush();
        std::getline(std::cin, in);
        if(Is_String_An_X<T>(in)) return stringtoX<T>(in);
    }
    FUNCERR("Should never get here");
    return T();
}

template int64_t Poll_Until_We_Get_A_<int64_t>(const std::string& msg);
template int32_t Poll_Until_We_Get_A_<int32_t>(const std::string& msg);
template uint64_t Poll_Until_We_Get_A_<uint64_t>(const std::string& msg);
template uint32_t Poll_Until_We_Get_A_<uint32_t>(const std::string& msg);
template double Poll_Until_We_Get_A_<double>(const std::string& msg);
template std::string Poll_Until_We_Get_A_<std::string>(const std::string& msg);

template<> time_mark Poll_Until_We_Get_A_<time_mark>(const std::string& msg){
    while(true){
        time_mark out;
        std::string in;
        std::cout << msg << std::endl;
        std::cout.flush();
        std::getline(std::cin, in);
        if(out.Read_from_string(in)){
            return out;
        //}else{
        //    FUNCWARN("'" << in << "' is not a valid date");
        }
    }
    FUNCERR("Should never get here");
    return time_mark();
}


bool Overwrite_Updated_Task(const Deadline &A){
    const auto fname = A.Filename_Origin;

    if(!Does_File_Exist_And_Can_Be_Read(fname)){
        FUNCWARN("The file associated with the chosen task has disappeared. Choosing not to write anything");
        return false;
    }

    //Ensure the user wants this. Maybe remove after debugging...
    std::string resp;
    do{
        resp = Poll_Until_We_Get_A_<std::string>("OK to overwrite file '"_s + fname + "'? [y/n]");
    }while((resp != "y") && (resp != "n"));

    if(resp == "n"){
        FUNCINFO("User aborted file overwrite");
        return false;
    }

    if(!(A.Write_To_File(fname))){
        FUNCWARN("Could not overwrite file");
        return false;
    }

    FUNCINFO("Updated/overwrote task in file '"_s + fname + "'"_s);
    return true;
}

std::string Get_Users_Home_Directory(void){
    //This routine should return the equivalent of an expanded '~/'.

    char const *home = getenv("HOME");
    if(home != nullptr) return std::string(home);

    home = getpwuid(getuid())->pw_dir;
    if(home != nullptr) return std::string(home);

    throw std::runtime_error("Unable to locate user's home directory.");
    return std::string();
}

std::string Expand_Tilde_Home_Directory(const std::string &in){
    if((in.size() >= 2) && (in.substr(0,2) == "~/")){
        return Get_Users_Home_Directory() + "/" + in.substr(2);
    }
    return in;
}

