//Email.h.

#include <string>
#include <iostream>

#include <curl/curl.h>

#include "Email.h"

bool Post_Email(const std::string &user,
                const std::string &password,
                const std::string &url,
                const std::string &from,
                const std::string &to,
                const std::string &subject,
                const std::string &body){

    CURL *curl = curl_easy_init();
    if(curl == nullptr) return false;

    curl_easy_setopt(curl, CURLOPT_USERNAME, user.c_str());
    curl_easy_setopt(curl, CURLOPT_PASSWORD, password.c_str());
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

    struct curl_httppost *post = nullptr;
    struct curl_httppost *last = nullptr;

    curl_formadd(&post, &last, CURLFORM_COPYNAME, "from",
                  CURLFORM_COPYCONTENTS, from.c_str(), CURLFORM_END);
    curl_formadd(&post, &last, CURLFORM_COPYNAME, "to",
                  CURLFORM_COPYCONTENTS, to.c_str(), CURLFORM_END);
    curl_formadd(&post, &last, CURLFORM_COPYNAME, "subject",
                  CURLFORM_COPYCONTENTS, subject.c_str(), CURLFORM_END);
    curl_formadd(&post, &last, CURLFORM_COPYNAME, "html",
                  CURLFORM_COPYCONTENTS, body.c_str(), CURLFORM_END);

    curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

    auto res = curl_easy_perform(curl); //Note: will print response to stdout/stderr.
    std::cout << std::endl;
    
    curl_easy_cleanup(curl);
    curl_formfree(post);
    return (res == CURLE_OK);
}

