//Detect_Phone_NA_Blocks.cc -- Detect time periods where your phone was not reachable by the ping logger.
//
// This program is helpful for logging events after they happen, such as walking the dog or going to work.
//

#include <iostream>
#include <limits>
#include <initializer_list>
#include <cmath>
#include <list>
#include <ctime>
#include <chrono>
#include <iomanip>

#include <pqxx/pqxx>

//#include "YgorMisc.h"
//#include "YgorMath.h"

struct sample {
    long int time;
    int value; 
};

int main(int argc, char **argv){


    //Query the db to get the phone ping info for the last while.
    std::vector<sample> samples;
    const bool inhibit_sort = true;
    try{
        pqxx::connection c("dbname=phone_ping_logging user=hal host=localhost port=63443");
        pqxx::work txn(c);

        const std::string query1(" SELECT "
                                 "    EXTRACT(EPOCH FROM pingtime)::INT AS time, "
                                 "    success::INT AS value "
                                 " FROM samsung_galaxy_note_3 "
                                 " WHERE ( pingtime > (now() - '5 week'::INTERVAL) ) "
                                 " ORDER BY pingtime ASC ;");
        pqxx::result r1 = txn.exec(query1);
        if(r1.size() == 0) throw std::runtime_error("Query1: no records found.");
        for(auto i = 0; i < r1.size(); ++i){
            if( r1[i]["time"].is_null() || r1[i]["value"].is_null() ){
                throw std::runtime_error("DB returned null data. This should not happen. Please inspect. -h.");
            }
            //std::cout << " Record: " << r1[i]["time"].as<double>() << "   " << r1[i]["value"].as<double>() << std::endl;
            samples.emplace_back();
            samples.back() = { r1[i]["time"].as<long int>(), r1[i]["value"].as<int>() };
        }

    }catch(const std::exception &e){
        std::cerr << "Encountered an error: " << e.what() << std::endl;
        return 1;
    }

    //Smooth the data so that noise will not be considered a level change.

    // ... Median filter?

    //Now step through the data and detect level changes.
    const auto epoch_time = std::chrono::time_point<std::chrono::high_resolution_clock>();
    
    auto last = samples.front();
    long int t_last_flip = -1;

    std::cout << std::endl;
    for(const auto &p : samples){
        const long int t_now = p.time; // In between last.time and p.time?

        //If there is a positive flip, check if the period is well-defined and report it.
        if( (last.value < 0.5)  &&  (p.value > 0.5) ){
            const auto duration = (t_now - t_last_flip);
            if( (t_last_flip > 0) //Handle the case of the open-ended block.
            &&  (duration > 10*60) ){ //Handle the case of suspected noise. Only care about longer blocks. 
                std::cout << "Found a block. dTime = " 
                          << std::round((t_now - t_last_flip) / 60.0) << " min, or " 
                          << (t_now - t_last_flip) / (60.0 * 60.0) << " h." << std::endl;

                const auto start_time = std::chrono::high_resolution_clock::to_time_t(epoch_time + std::chrono::seconds(t_last_flip));
                const auto   end_time = std::chrono::high_resolution_clock::to_time_t(epoch_time + std::chrono::seconds(t_now));

                std::cout << "\t\t Work Began = " << std::put_time(std::gmtime(&start_time), "%Y%m%d-%H%M%S") << std::endl;
                std::cout << "\t\t Work Ended = " << std::put_time(std::gmtime(&end_time  ), "%Y%m%d-%H%M%S") << std::endl;

                std::cout << std::endl;

            }

        //Negative flip. Record the time.
        }else if( (last.value > 0.5)  &&  (p.value < 0.5) ){
            t_last_flip = t_now;
        }

        last = p;
    }


    std::cout << "   ^^^^^^^^^^^^^ most recent ^^^^^^^^^^^^^ " << std::endl;
    return 0;
}

