//Analyze_Tasks.cc - Provides analytical operations for Disciplinarian tasks.
//

#include <cstdint> //For intmax_t

#include <list>
#include <string>
#include <memory>
#include <vector>
#include <iomanip>
#include <functional>
#include <unistd.h>    //Needed for execve().
#include <sstream>
#include <algorithm>

#include <boost/program_options.hpp>

#include "YgorMisc.h"
#include "YgorAlgorithms.h"
#include "YgorEnvironment.h"
#include "YgorSerialize.h"
#include "YgorString.h"
#include "YgorArguments.h"

#include "Structs.h"
#include "Helpers.h"
#include "Email.h"

enum EmphWith { Colour, Bold, None };

uint64_t Total_Est_Length(const std::list<Deadline> &in){
    uint64_t out = 0;
    for(auto it=in.begin(); it!=in.end(); ++it) out += it->Estimated_Length;
    return out;
}

double Total_Priority(const std::list<Deadline> &in){
    double out = 0.0;
    for(auto it=in.begin(); it!=in.end(); ++it) out += it->Get_Current_Priority();
    return out;
}

double Total_Work_Done_On_Day(const std::list<Deadline> &in, const time_mark &tday){
    double out = 0.0;
    for(auto it=in.begin(); it!=in.end(); ++it) out += it->Get_Total_Working_Time_On_Day(tday);
    return out;
}

std::string Print_First_N_Tasks(const std::list<Deadline> &in, size_t N, EmphWith e = EmphWith::None){
    std::stringstream ss;
    size_t n = 1;
    auto l_it = in.begin();
    while( (l_it != in.end()) && (n <= N) ){

        //Print the task number, priority, title, end date, and filename.
        ss << std::setw(2) << n;
        ss << " - " << std::fixed << std::setprecision(5) << l_it->Get_Current_Priority();
        ss << " - " << std::fixed << std::setw(5) << l_it->Estimated_Length;
        ss << " - " << std::fixed << std::setw(5) << std::setprecision(0) << l_it->Get_Current_Work_Time();
        ss << " - " << l_it->End.Dump_as_string();
        ss << " - " << std::setw(10) << Get_Bare_Filename(l_it->UID);
        ss << " - ";
        if(false){
        }else if(e == EmphWith::Colour){
            ss << PrintYellow(l_it->Title);
        }else if(e == EmphWith::Bold){
            ss << "<b>" << l_it->Title << "</b>";
        }else{
            ss << l_it->Title;
        }
        ss << std::endl;
 
        ++l_it;
        ++n;
    }
    return ss.str();
}

int main(int argc, char **argv){
    
    std::stringstream ss;

    //Environment variables.
    const uint32_t override_usage_term_width = 160;
    const uint32_t override_usage_reserve_width = 50;

    const std::string progname(argv[0]);

    const std::vector<std::string> Possible_Config_Files {   
        //Highest-preference files first.
        (Get_Users_Home_Directory() + "/.disciplinarian.conf"),
        "/etc/disciplinarian.conf" 
    };

    //Program variables and behavioural switches.
    std::list<Deadline> Tasks;

    std::string Base_Dir;

    EmphWith EmphType = EmphWith::None;
    bool UseEmph; //Enables emphasis (colour or bold), but type is dictated by output format.

    bool Send_Email;
    std::string email_user;
    std::string email_password;
    std::string email_url;
    std::string email_from;
    std::string email_to;
    std::string email_subject;
    std::string email_body;

    // Argument handling and config file parsing.
    {
        namespace po = boost::program_options;

        po::options_description RegularOptions("General Options",
                                               override_usage_term_width,
                                               override_usage_reserve_width);
        RegularOptions.add_options()
           ( "help,h",
             "  Display usage.\n" )
           ( "base-dir,B",
             po::value< std::string >(&Base_Dir),
             "  The path to the root directory of the task store."
              " Note that Bash syntax '~/' is supported as long as the 'HOME' environment variable is defined.\n"
             "  \tExamples:\n"
             "  \t  '~/disciplinarian_store/'\n"
             "  \t  '~/disciplinarian_store/'\n"
             "  \t  '/home/someuser/disciplinarian_store/'\n"
             "  \t  '~/.task_store/'\n" )
           ( "use-emph,u",
             po::bool_switch(&UseEmph)->default_value(false),
             "  Enable emphasis for titles. (Note: whether to use colour or HTML bold is controlled by email flag.)\n" )
           ( "send-email,s",
             po::bool_switch(&Send_Email)->default_value(false),
             "  Send an email via POST notification."
              " Note that this option will disable terminal output."
              " This option is most useful when used with email-as-a-service.\n" )
           ( "email.user",
             po::value< std::string >(&email_user),
             "  Username to supply for email POST notification.\n" )
           ( "email.password",
             po::value< std::string >(&email_password),
             "  Password to supply for email POST notification.\n" )
           ( "email.url",
             po::value< std::string >(&email_url),
             "  URL for email POST notification.\n" )
           ( "email.from",
             po::value< std::string >(&email_from),
             "  'From' email address for email POST notification.\n" )
           ( "email.to",
             po::value< std::string >(&email_to),
             "  Recipient's email address for email POST notification.\n" )
           ( "email.subject",
             po::value< std::string >(&email_subject),
             "  Optionally override the autogenerated subject for email POST notification.\n" )
        ;

        po::positional_options_description PositionalOptions;
        PositionalOptions.add("uri", -1); //Assume all un-marked options are URIs ... needed ??? TODO ???.

        // Parse command line options.
        po::variables_map VarMap;
        po::store(
            po::command_line_parser(argc, argv)
              .options(RegularOptions)
              .positional(PositionalOptions)
              .allow_unregistered()
              .run(),
            VarMap);

        // Parse any readable config files.
        for(const auto &f : Possible_Config_Files){
            std::ifstream fif(f);
            if(fif){
                const bool IgnoreUnregistered = true;
                po::store(po::parse_config_file(fif, RegularOptions, IgnoreUnregistered), VarMap);
            }
        }

        po::notify(VarMap);

        //Print usage.
        if(VarMap.count("help")){
            // ... = VarMap["show-complete"].as<bool>();
            std::cout << progname << " -- A part of Disciplinarian." << std::endl;
            std::cout << RegularOptions << std::endl;
            return 0;
        }

    }

    //Argument post-processing.
    if(Base_Dir.empty()){
        throw std::invalid_argument("No base directory provided. Cannot continue.");
    }
    Base_Dir = Expand_Tilde_Home_Directory(Base_Dir);

    if(UseEmph){
        if(Send_Email){ 
            EmphType = EmphWith::Bold;
        }else{
            EmphType = EmphWith::Colour;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    //Find all tasks in the store. Try to read them all.
    const auto Active_Dir   = Remove_Trailing_Chars(Base_Dir,"/") + "/Active/";
    const auto Inactive_Dir = Remove_Trailing_Chars(Base_Dir,"/") + "/Inactive/";
    auto Filenames = Get_List_of_Full_Path_File_Names_in_Dir(Active_Dir);
    if(!Append_List_of_Full_Path_File_Names_in_Dir(Filenames,Inactive_Dir)){
        throw std::runtime_error("Unable to access '" + Inactive_Dir + "'. Not continuing");
    }
    Filenames.sort();
    std::unique(std::begin(Filenames), std::end(Filenames));

    for(auto l_it = Filenames.begin(); l_it != Filenames.end(); ++l_it){
        Tasks.push_back(Deadline()); //Attempt to parse.

        const auto suffix = Get_Trailing_Chars(*l_it, 4);
        if(".TXT" != Canonicalize_String2(suffix,CANONICALIZE::TO_UPPER)){ //Ignore vim swap files.
            FUNCWARN("Ignoring file '" << *l_it << "'");
            Tasks.pop_back();
        }else if(!(Tasks.back().Read_From_File(*l_it))){ //Ignore if it cannot be read or parsed properly.
            Tasks.pop_back();
        }
    }

    if(Tasks.empty()){
        FUNCWARN("No available tasks. Ensure this is not a mistake");
        return 0;
    }

    // Things to show:
    // - The next 5 tasks that will expire (and the amount of time until each expire).
    // - Work accomplished yesterday (time spent, tasks worked on, repo change log, tasks completed).    

    Tasks.sort(Deadline_Sort_Compare_Higher_Priority_First);

    //Remind the user which tasks are currently being worked on.
    {
        auto TCopy = Tasks;
        TCopy.erase(std::remove_if(TCopy.begin(), TCopy.end(),
            [](const Deadline &t){ 
                return not t.Is_Curr_Worked_On(); 
            }), 
            TCopy.end());

        TCopy.sort(Deadline_Sort_Compare_Higher_Priority_First);
        if(not TCopy.empty()){
            ss << "=== Tasks currently being worked on ===" 
               << std::endl
               << Print_First_N_Tasks(TCopy, 10000000, EmphType)
               << std::endl;
        }
    }

    //List the top few tasks which were most recently worked on.
    {
        auto TCopy = Tasks;
        TCopy.sort(Deadline_Sort_Compare_Most_Recently_Worked_On_First);
        ss << "=== Top 10 most recently worked on tasks ===" 
           << std::endl
           << Print_First_N_Tasks(TCopy, 10, EmphType)
           << std::endl;
    }

 
    //List the top few highest-priority (active) tasks.
    {
        auto TCopy = Tasks;
        TCopy.sort(Deadline_Sort_Compare_Higher_Priority_First);
        ss << "=== Top 10 highest-priority tasks ===" 
           << std::endl
           << Print_First_N_Tasks(TCopy, 10, EmphType)
           << std::endl;
    }

    //List the oldest few (active, non-zero priority) tasks.
    {
        auto TCopy = Tasks;
        TCopy.erase(std::remove_if(TCopy.begin(), TCopy.end(),
            [](const Deadline &t){ 
                return not (t.Status == dspln::status::active); 
            }), 
            TCopy.end());

        TCopy.sort(Deadline_Sort_Compare_Older_First);
        if(not TCopy.empty()){
            ss << "=== Top 10 oldest active tasks ===" 
               << std::endl
               << Print_First_N_Tasks(TCopy, 10, EmphType)
               << std::endl;
        }
    }
    
    //List the shortest few (active, non-zero priority) tasks.
    {
        auto TCopy = Tasks;
        TCopy.erase(std::remove_if(TCopy.begin(), TCopy.end(),
            [](const Deadline &t){ 
                return not ( (t.Status == dspln::status::active) 
                             && (t.Estimated_Length > 0) );
            }), 
            TCopy.end());

        TCopy.sort(Deadline_Sort_Compare_Short_First);
        if(not TCopy.empty()){
            ss << "=== Top 10 shortest active tasks (>=1min) ===" 
               << std::endl
               << Print_First_N_Tasks(TCopy, 10, EmphType)
               << std::endl;
        }
    }
   
    //List the top few tasks with the fastest growing importance.
    {
        auto TCopy = Tasks;
        TCopy.sort(Deadline_Sort_Compare_Highest_Importance_Derivative_First);
        ss << "=== Top 10 fastest-growing importance tasks ===" 
           << std::endl
           << Print_First_N_Tasks(TCopy, 10, EmphType)
           << std::endl;
    }

    //List the top few active tasks that are close to their deadlines.
    {
        auto TCopy = Tasks;
        TCopy.erase(std::remove_if(TCopy.begin(), TCopy.end(),
            [](const Deadline &t){ 
                return not (t.Status == dspln::status::active);
            }), 
            TCopy.end());

        TCopy.sort(Deadline_Sort_Compare_Closest_To_Deadline_First);
        if(not TCopy.empty()){
            ss << "=== Top 10 active tasks nearest their deadline (pre or post) ===" 
               << std::endl
               << Print_First_N_Tasks(TCopy, 10, EmphType)
               << std::endl;
        }
    }

    //List a few of the most important tasks that have not been tagged.
    {
        auto TCopy = Tasks;
        TCopy.erase(std::remove_if(TCopy.begin(), TCopy.end(),
            [](const Deadline &t){ 
                return t.Tags.empty();
            }), 
            TCopy.end());

        if(not TCopy.empty()){
            ss << "=== Top 10 most important tasks without any tags ===" 
               << std::endl
               << Print_First_N_Tasks(TCopy, 10, EmphType)
               << std::endl;
        }
    }



    //Stats dumping.
    {
        const auto N = Tasks.size();

        ss << "Number of tasks considered:             "
           << N << std::endl;

        const auto TotEstLen = Total_Est_Length(Tasks);
        ss << "Est. length of all tasks:               "
           << TotEstLen << " min, or " << TotEstLen/60 << " hours, or "
           << TotEstLen/60/8 << " 8-hour-days" 
           << std::endl;

        ss << "Avg est. length of all tasks:           "
           << TotEstLen/N << " min, or " << TotEstLen/N/60 << " hours"
           << std::endl;

        const auto TotPri = Total_Priority(Tasks);
        ss << "Total priority of tasks:                "
           << TotPri << std::endl;

        ss << "Avg. priority of tasks:                 "
           << TotPri/N << std::endl;

        time_mark tspec;
        for(size_t i=0; i<=7; ++i){
            //Spit out the day's working times.
            const auto TotWorkYest = Total_Work_Done_On_Day(Tasks,tspec);
            ss << "Total (indep.) Work done " << std::setw(2) << i << " days back:   "
               << std::fixed << std::setw(5) << std::setprecision(0)
               << TotWorkYest << " min, or "
               << std::fixed << std::setw(5) << std::setprecision(1)
               << TotWorkYest/60.0 << " hours"
               << std::endl;

            //Spit out the specific tasks worked on.
            for(auto it=Tasks.begin(); it!=Tasks.end(); ++it){
                const auto dt = it->Get_Total_Working_Time_On_Day(tspec);
                if(dt <= 0.0) continue;
                ss << "        "
                   << std::fixed << std::setw(5) << std::setprecision(0) << dt
                   << " ";
                if(false){
                }else if(EmphType == EmphWith::Colour){
                    ss << PrintYellow(it->Title);
                }else if(EmphType == EmphWith::Bold){
                    ss << "<b>" << it->Title << "</b>";
                }else{
                    ss << it->Title;
                }
                ss << std::endl;
            }
            tspec.Regress_One_Day();
        }

    }

    if(Send_Email){
        if(email_subject.empty()){
            time_mark t;
            email_subject = "Disciplinarian Tasks " + t.Dump_as_string();
        }
        email_body = "<html><body><pre>" + ss.str() + "</html></body></pre>";
        if(!Post_Email(email_user, email_password, email_url, email_from, email_to, email_subject, email_body)){
            throw std::runtime_error("Unable to send email...");
        }
    }else{
        std::cout << ss.str();
    }


    return 0;
}
