//Helpers.h.

#ifndef PROJECT_DISCIPLINARIAN_HELPERS_H_HDR_GRD
#define PROJECT_DISCIPLINARIAN_HELPERS_H_HDR_GRD 

#include <cstdint> //For intmax_t

#include <list>
#include <string>
#include <memory>
#include <vector>
#include <iomanip>
#include <functional>
#include <unistd.h>    //Needed for execve().

#include "YgorMisc.h"
#include "YgorAlgorithms.h"
#include "YgorEnvironment.h"
#include "YgorSerialize.h"
#include "YgorString.h"
#include "YgorArguments.h"

#include "Structs.h"



template <class T> T Poll_Until_We_Get_A_(const std::string& msg);

bool Overwrite_Updated_Task(const Deadline &A);

//This class is useful for wrapping text on the way to the terminal with a colour.
// Just wrap the text with a class instantiation:
//
//                   ... << "should be in yellow" << ... 
//                                   |
//                                  \|
//                                   `
//              ... << PrintYellow("should be in yellow") << ...
//
//It could be moved to Ygor. Maybe there is a more universal way though?
class PrintYellow {
    private:
        std::string buff;

    public:
        //PrintYellow(const std::stringstream &in) { buff += in.str(); }  //Compiles, but doesn't work (won't auto-convert str to stream).
        PrintYellow(const std::string &in) { buff += in; }

        friend std::ostream & operator<<(std::ostream &ss, const PrintYellow &a){
            const std::string ColBgn("\033[33m"); //Yellow.
            const std::string ColEnd("\033[0m"); //Reset.
            ss << ColBgn << a.buff << ColEnd;
            return ss;
        }
};

class PrintRed {
    private:
        std::string buff;

    public:
        //PrintRed(const std::stringstream &in) { buff += in.str(); }  //Compiles, but doesn't work (won't auto-convert str to stream).
        PrintRed(const std::string &in) { buff += in; }

        friend std::ostream & operator<<(std::ostream &ss, const PrintRed &a){
            const std::string ColBgn("\033[31m"); //Red.
            const std::string ColEnd("\033[0m"); //Reset.
            ss << ColBgn << a.buff << ColEnd;
            return ss;
        }
};


std::string Get_Users_Home_Directory(void);


std::string Expand_Tilde_Home_Directory(const std::string &);


#endif
