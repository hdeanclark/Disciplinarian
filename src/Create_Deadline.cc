//Create_Deadline.cc - The core program for injecting deadlines into the database
// of Project - Disciplinarian.
//
// This program is an interactive program which guides the user through creation
// of a deadline. Upon creation, it is appended to the database file.
//
#include <cstdint> //For intmax_t

#include <list>
#include <string>
#include <memory>

#include "YgorMisc.h"
#include "YgorAlgorithms.h"
#include "YgorSerialize.h"
#include "YgorString.h"
#include "YgorTime.h"
#include "YgorFilesDirs.h"

#include <boost/program_options.hpp>

#include "Structs.h"

#include "Helpers.h"


int main(int argc, char **argv){
 
    const std::vector<std::string> Possible_Config_Files {   
        //Highest-preference files first.
        (Get_Users_Home_Directory() + "/.disciplinarian.conf"),
        "/etc/disciplinarian.conf" 
    };

    std::string Base_Dir;

    // Argument handling and config file parsing.
    {
        namespace po = boost::program_options;

        po::options_description RegularOptions("General Options");

        RegularOptions.add_options()
           ( "help,h",
             "  Display usage.\n" )
           ( "base-dir,B",
             po::value< std::string >(&Base_Dir),
             "  The path to the root directory of the base/active task store."
              " Note that Bash syntax '~/' is supported as long as the 'HOME' environment variable is defined.\n"
             "  \tExamples:\n"
             "  \t  '~/disciplinarian_store/'\n"
             "  \t  '~/disciplinarian_store/'\n"
             "  \t  '/home/someuser/disciplinarian_store/'\n"
             "  \t  '~/.task_store/'\n" )
        ;

        po::positional_options_description PositionalOptions;
        PositionalOptions.add("uri", -1); //Assume all un-marked options are URIs ... needed ??? TODO ???.

        // Parse command line options.
        po::variables_map VarMap;
        po::store(
            po::command_line_parser(argc, argv)
              .options(RegularOptions)
              .positional(PositionalOptions)
              .allow_unregistered()
              .run(),
            VarMap);

        // Parse any readable config files.
        for(const auto &f : Possible_Config_Files){
            std::ifstream fif(f);
            if(fif){
                const bool IgnoreUnregistered = true;
                po::store(po::parse_config_file(fif, RegularOptions, IgnoreUnregistered), VarMap);
            }
        }

        po::notify(VarMap);

        //Print usage.
        if(VarMap.count("help")){
            // ... = VarMap["show-complete"].as<bool>();
            std::cout << RegularOptions << std::endl;
            return 0;
        }
    }

    //Argument post-processing.
    if(Base_Dir.empty()){
        throw std::invalid_argument("No base directory provided. Cannot continue.");
    }
    Base_Dir = Expand_Tilde_Home_Directory(Base_Dir);

    const auto Active_Dir   = Remove_Trailing_Chars(Base_Dir,"/") + "/Active/";
    const auto Inactive_Dir = Remove_Trailing_Chars(Base_Dir,"/") + "/Inactive/";

    //Create the store directories if they don't exist.
    if(!Does_Dir_Exist_And_Can_Be_Read(Active_Dir) && !Create_Dir_and_Necessary_Parents(Active_Dir)){
        throw std::runtime_error("Unable to create store at '" + Active_Dir + "'");
    }
    if(!Does_Dir_Exist_And_Can_Be_Read(Inactive_Dir) && !Create_Dir_and_Necessary_Parents(Inactive_Dir)){
        throw std::runtime_error("Unable to create store at '" + Inactive_Dir + "'");
    }

    ////////////////////////////////////////////////////

    Deadline theDeadline;
    theDeadline.Status = dspln::status::active;

    //Editing begin-time == Work Began time.
    const auto editing_began_time = time_mark();

    //Query the user for the title.
    std::string userin;
    std::cout << "What is the title of the task? When you are done, give me an empty line." << std::endl;
    std::cout.flush();
    theDeadline.Title.clear();
    do{
        std::getline(std::cin, userin);
        if(theDeadline.Title.empty()){
            theDeadline.Title += userin;
        }else if(!userin.empty()){
            theDeadline.Title += "\n"_s + userin;
        }
    }while(!userin.empty());


    //Query the user for the description.
    std::cout << "What is the description of the task? When you are done, give me an empty line." << std::endl;
    std::cout.flush();
    theDeadline.Description.clear();
    do{
        std::getline(std::cin, userin);
        if(theDeadline.Description.empty()){
            theDeadline.Description += "  "_s + userin;
        }else if(!userin.empty()){
            theDeadline.Description += "\n  "_s + userin;
        }
    }while(!userin.empty());

    //Query the user for any extra notes.
    std::cout << "Any additional notes? When you are done, give me an empty line." << std::endl;
    std::cout.flush();
    theDeadline.Notes.clear();
    do{
        std::getline(std::cin, userin);
        if(theDeadline.Notes.empty()){
            theDeadline.Notes += "  "_s + userin;
        }else if(!userin.empty()){
            theDeadline.Notes += "\n  "_s + userin;
        }
    }while(!userin.empty());

    //Query the user for the schedule.
    std::cout << "What sort of importance/priority schedule is this task? Currently available:" << std::endl;
    std::cout << " [ 1] - linear" << std::endl;
    std::cout << " [ 2] - sigmoidal (low priority initially)" << std::endl;
    std::cout << " [ 3] - Gaussian, half-max at: end - 1 day" << std::endl;
    std::cout << " [ 4] - Gaussian, half-max at: end - 3 days" << std::endl;
    std::cout << " [ 5] - Gaussian, half-max at: end - 1 week" << std::endl;
    std::cout << " [ 6] - Gaussian, half-max at: end - 2 weeks" << std::endl;
    std::cout << " [ 7] - Gaussian, half-max at: end - 1 month" << std::endl;
    std::cout << " [ 8] - Gaussian, half-max at: end - 2 months" << std::endl;
    std::cout << " [ 9] - Gaussian, half-max at: end - 2 x estimated length" << std::endl;
    std::cout << " [10] - Gaussian, half-max at: end - 20 x estimated length" << std::endl;
    std::cout << " [11] - Gaussian, half-max at: end - 200 x estimated length" << std::endl;
    while(true){
        const auto N = Poll_Until_We_Get_A_<uint64_t>("Your numeric choice: ");
        if(      N ==  1){  theDeadline.Schedule = dspln::sched::linear;                break;
        }else if(N ==  2){  theDeadline.Schedule = dspln::sched::sigmoidal;             break;
        }else if(N ==  3){  theDeadline.Schedule = dspln::sched::gaussian_em1day;       break;
        }else if(N ==  4){  theDeadline.Schedule = dspln::sched::gaussian_em3days;      break;
        }else if(N ==  5){  theDeadline.Schedule = dspln::sched::gaussian_em1week;      break;
        }else if(N ==  6){  theDeadline.Schedule = dspln::sched::gaussian_em2weeks;     break;
        }else if(N ==  7){  theDeadline.Schedule = dspln::sched::gaussian_em1mon;       break;
        }else if(N ==  8){  theDeadline.Schedule = dspln::sched::gaussian_em2mons;      break;
        }else if(N ==  9){  theDeadline.Schedule = dspln::sched::gaussian_em2xestlen;   break;
        }else if(N == 10){  theDeadline.Schedule = dspln::sched::gaussian_em20xestlen;  break;
        }else if(N == 11){  theDeadline.Schedule = dspln::sched::gaussian_em200xestlen; break;
        }else{
            FUNCWARN(N << " is not a valid choice");
        }
    }

    //Query for the initial and final importance.
    std::cout << "What initial importance should we assume? [0.0,1.0]" << std::endl;
    while(true){
        const auto X = Poll_Until_We_Get_A_<double>("Initial importance: ");
        if((X >= 0.0) && (X <= 1.0)){
            theDeadline.Ii = X;
            break;
        }else{
            FUNCWARN(X << " is not a valid importance");
        }
    }

    std::cout << "What final importance should we assume? [0.0,1.0]" << std::endl;
    while(true){
        const auto X = Poll_Until_We_Get_A_<double>("Final importance: ");
        if((X >= 0.0) && (X <= 1.0) && (X < theDeadline.Ii)){
            FUNCWARN("Final importance must be >= initial importance");
        }else if((X >= 0.0) && (X <= 1.0)){
            theDeadline.If = X;
            break;
        }else{
            FUNCWARN(X << " is not a valid importance");
        }
    }

    //What is the expected time (in minutes)?
    std::cout << "Roughly how many minutes do expect this to take? [1,...)" << std::endl;
    std::cout << " Note: If (and only if) you cannot guess, enter '0'" << std::endl;
    while(true){
        const auto X = Poll_Until_We_Get_A_<uint64_t>("Number of minutes: ");
        if( (  (theDeadline.Schedule == dspln::sched::gaussian_em2xestlen) 
            || (theDeadline.Schedule == dspln::sched::gaussian_em20xestlen) 
            || (theDeadline.Schedule == dspln::sched::gaussian_em200xestlen) )
        &&  (X == 0) ){   
            FUNCWARN("The schedule you have chosen requires an estimate of the required time");
        }else if(X < 80000){ //Roughly two months.
            theDeadline.Estimated_Length = X;
            break;
        }else{
            FUNCWARN(X << " is a very long time... Please edit the file directly afterward");
        }
    }

    //Dump a calendar.
    const auto acal = Execute_Command_In_Pipe("cal -y --color=always");
    std::cout << acal;
    std::cout << std::endl;

    //Query the user for a deadline. We use a time_mark, and thus offload date/time parsing.
    std::cout << "When is the deadline? Use format: '" << time_mark().Dump_as_string() << "'" << std::endl;
    while(true){
        const auto userdatetime = Poll_Until_We_Get_A_<std::string>("Deadline: ");
        if(theDeadline.End.Read_from_string(userdatetime)){
            break;
        }else{
            FUNCWARN("'" << userdatetime << "' is not a valid date");
        }
    }

    //Query the user for tags.
    std::cout << "What are some tags that describe the task? One per line. When you are done, give me an empty line." << std::endl;
    std::cout << "Existing tags: " << std::endl;
    std::cout << Execute_Command_In_Pipe("disciplinarian_get_all_existing_tags.sh " + Quote_Static_for_Bash(Base_Dir));
    std::cout << std::endl;
    std::cout.flush();
    theDeadline.Tags.clear();
    do{
        std::getline(std::cin, userin);
        userin = Canonicalize_String2(userin, CANONICALIZE::TRIM);
        if(theDeadline.Tags.empty()){
            theDeadline.Tags += "  "_s + userin;
        }else if(!userin.empty()){
            theDeadline.Tags += "\n  "_s + userin;
        }
    }while(!userin.empty());


    //Insert the work time spent creating + editing this task. Editing end-time == Work Ended time.
    const auto editing_ended_time = time_mark();
    theDeadline.Mark_Work_Began_At(editing_began_time);
    theDeadline.Mark_Work_Ended_At(editing_ended_time);

    //Try to come up with a unique filename based on the base dir and title.
    const auto SafeFilename = Detox_String(theDeadline.End.Dump_as_string() + "_"_s + theDeadline.Title).substr(0,40);
    const auto Filename_Out = Get_Unique_Filename(Active_Dir + SafeFilename, 4, ".txt");
    if(!theDeadline.Write_To_File(Filename_Out)){
        FUNCERR("Could not write to file");
    }else{
        FUNCINFO("Wrote to file '" << Filename_Out << "'");
    }

    //Exec to editing the task.
    {
        FUNCINFO("execve-ing to vim. Goodbye");

        //These const casts are OK - we are either exec'ing or dying.
        char *thefilename = const_cast<char *>(Filename_Out.c_str());
        char *basedir = const_cast<char *>(Base_Dir.c_str());
        char theeditor[] = "disciplinarian_create_edit_commit.sh"; //execlp will perform path resolution for us.
        if(-1 == execlp(theeditor,theeditor,thefilename,basedir,(char *)nullptr)){
            FUNCERR("Couldn't execve vim. The filename is '" << Filename_Out << "'");
        }//Does not return on success - overwrites the stack.
    }

    return 0;
}

