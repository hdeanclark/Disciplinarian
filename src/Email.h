//Email.h.

#pragma once

#include <string>

bool Post_Email(const std::string &user,
                const std::string &password,
                const std::string &url,
                const std::string &from,
                const std::string &to,
                const std::string &subject,
                const std::string &body);

