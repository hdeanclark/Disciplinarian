//Structs.cc

#include <string>
#include <memory>
#include <ctime>
#include <cmath>
#include <limits>
#include <sstream>
#include <map>
#include <random>

#include "YgorMisc.h"
#include "YgorTime.h"
#include "YgorSerialize.h"
#include "YgorFilesDirs.h"
#include "YgorString.h"

#include "Structs.h"

//Constructor, Destructor.
Deadline::Deadline(){
    this->Populate_Reasonable_Defaults();
}

//Member functions.
void Deadline::Populate_Reasonable_Defaults(){
    //Some sensible defaults.
    this->Status = dspln::status::active;
    this->Schedule = dspln::sched::linear;
    this->Estimated_Length = 0;

    this->A = this->B = this->C = this->D = 0.0;
    this->Ii = 0.0;
    this->If = 1.0;

    time_mark tnow; //Guaranteed to be current time.
    this->Begin = tnow;
    this->End = time_mark().One_Day_Later_Than( tnow );
    this->Completed = time_mark().One_Day_Earlier_Than( tnow );

    this->Working.clear();

    this->UID = std::string("DT") + Generate_Random_String_of_Length(7);
    this->Tags = "Unassigned";
    this->Title = "Unnamed Task";
    this->Description = "This is the default task description.";
    this->Notes.clear();
    this->Filename_Origin.clear();
    return;
}


double Deadline::Get_Priority_At(const time_mark &tspec) const { //Evaluates the current importance.
    //Note: to add schedules, just provide an f s.t. f(X=0)~=0 and f(X=1)~=1.
    // The flip cases (decreasing importance) will be handled by pre-flipping
    // X if needed.
    //
    //Note: For synthesizing schedules, here is some useful gnuplot stuff:
    //     set xrange [0:1] ; set yrange [0:1] ; set term dumb size 80 ;
    if(this->Status == dspln::status::completed) return 0.0;
    if(this->Status == dspln::status::abandoned) return 0.0;

    // Times used here:
    //        |---------------------------------------|--------------------|
    //        .                                       .                    .
    //       /|\                                     /|\                  /|\
    //        |                                       |                    |
    //    Beginning                          The specified time           End
    //                                        (e.g., right now)
    // Timespans used here:
    //        |>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>|>>>>>>>>>>>>>>>>>>>>|
    //                          t1                              t2
    //     
    //        |>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>|
    //                                     T
    //                                                      |>>>>>>>>>>>>>>|
    //                                                          t3 (i.e., 
    //                                                        Estimated Length)
    //                                                |>>>>>|
    //                                                   t4
    // Descriptions:
    //  t1 = elapsed time since task began.
    //  t2 = remaining time until task ends.
    //  t3 = estimated task time, provided by human. Can be 0.
    //  t4 = "remaining free time". After this time, user should work on the task.
    //
    const auto t1 = this->Begin.Diff_in_Seconds( tspec );
    const auto t2 = tspec.Diff_in_Seconds( this->End );
    const auto t3 = static_cast<double>(this->Estimated_Length);
    const auto t4 = t2 - t3;   

    if(t1 < 0) return this->Ii; //If before beginning time, lowest priority.
    if(t2 < 0) return this->If; //If past end time, highest priority.

    //Otherwise, importance depends on the schedule. 
    const auto T = this->Begin.Diff_in_Seconds( this->End );
    if(T == 0) FUNCERR("Begin and End dates match. Cannot continue");
    const auto Xraw = static_cast<double>(t1)/static_cast<double>(T); //Range: [0,1]
    const auto dI = this->If - this->Ii;
    const auto X = (dI >= 0.0) ? Xraw : (1.0 - Xraw); //Handles decreasing importance case.

    double Iraw(0.0);
    if(this->Schedule == dspln::sched::linear){
        //Simple linear schedule. 
        Iraw = X;

    }else if(this->Schedule == dspln::sched::sigmoidal){
        //Sigmoidal schedule with the half-max at X=0.5 (i.e., centered).
        //
        //    1 ++------------+-------------+------------+-------------+---***********
        //      +             +             +    1.0/(1.0+exp(-10.0*(x-0.5))) ****** +
        //  0.9 ++                                              ****                ++
        //      |                                            ****                    |
        //  0.8 ++                                         ***                      ++
        //      |                                        ***                         |
        //  0.7 ++                                     ***                          ++
        //      |                                    ***                             |
        //  0.6 ++                                  **                              ++
        //  0.5 ++                                 **                               ++
        //      |                                **                                  |
        //  0.4 ++                              **                                  ++
        //      |                             ***                                    |
        //  0.3 ++                          ***                                     ++
        //      |                         ***                                        |
        //  0.2 ++                      ***                                         ++
        //      |                    ****                                            |
        //  0.1 ++                ****                                              ++
        //      +         *********         +            +             +             +
        //    0 ***********---+-------------+------------+-------------+------------++
        //      0            0.2           0.4          0.6           0.8            1
        //
        // Increasing the scaling factor makes is more 'curvy', but X=0.5 always -> Y=0.5.
        Iraw = 1.0/(1.0+std::exp(-12.0*(X-0.5)));

    }else if(this->Schedule == dspln::sched::gaussian_em1day){
        //Gaussian schedule with the half-maximum at Tend - 1 day.
        //Note: The Gaussian is one-sided. It will not decrease after peaking.
        //Note: This diagram assumes T is 5 days long: the day before will be at 0.5.
        //      Two days before will be at about 0.06.
        //
        //    1 ++------------+-------------+------------+-------------+-----------***
        //      +             +             +            +             +         **  +
        //      |             :             :            :             :       **    |
        //      |             :             :            :             :      **     |
        //  0.8 ++...........................................................*......++
        //      |             :             :            :             :    *        |
        //      |             :             :            :             :   *         |
        //      |             :             :            :             :  *          |
        //  0.6 ++......................................................**..........++
        //      |             :             :            :             :*            |
        //      |             :             :            :            **             |
        //  0.4 ++....................................................*.............++
        //      |             :             :            :          ** :             |
        //      |             :             :            :         **  :             |
        //      |             :             :            :       **    :             |
        //  0.2 ++..............................................**..................++
        //      |             :             :            :    **       :             |
        //      |             :             :            : ***         :             |
        //      +             +             +         ******           +             +
        //    0 **************************************---+-------------+------------++
        //      0            0.2           0.4          0.6           0.8            1
        //
        const auto lT = static_cast<double>(T);
        const auto GXraw = (lT - static_cast<double>(24*60*60))/lT;
        const auto SS = (GXraw - 1.0)*(GXraw - 1.0)/std::log(2.0); //sigma^2.
        Iraw = std::exp(-(X-1.0)*(X-1.0)/SS);
 
    }else if(this->Schedule == dspln::sched::gaussian_em3days){
        //Very similar to gaussian_em1day, but with the time shift altered.
        const auto lT = static_cast<double>(T);
        const auto GXraw = (lT - static_cast<double>(3*24*60*60))/lT;
        const auto SS = (GXraw - 1.0)*(GXraw - 1.0)/std::log(2.0); //sigma^2.
        Iraw = std::exp(-(X-1.0)*(X-1.0)/SS);

    }else if(this->Schedule == dspln::sched::gaussian_em1week){
        //Very similar to gaussian_em1day, but with the time shift altered.
        const auto lT = static_cast<double>(T);
        const auto GXraw = (lT - static_cast<double>(7*24*60*60))/lT;
        const auto SS = (GXraw - 1.0)*(GXraw - 1.0)/std::log(2.0); //sigma^2.
        Iraw = std::exp(-(X-1.0)*(X-1.0)/SS);

    }else if(this->Schedule == dspln::sched::gaussian_em2weeks){
        //Very similar to gaussian_em1day, but with the time shift altered.
        const auto lT = static_cast<double>(T);
        const auto GXraw = (lT - static_cast<double>(2*7*24*60*60))/lT;
        const auto SS = (GXraw - 1.0)*(GXraw - 1.0)/std::log(2.0); //sigma^2.
        Iraw = std::exp(-(X-1.0)*(X-1.0)/SS);

    }else if(this->Schedule == dspln::sched::gaussian_em1mon){
        //Very similar to gaussian_em1day, but with the time shift altered.
        const auto lT = static_cast<double>(T);
        const auto GXraw = (lT - static_cast<double>(30*24*60*60))/lT;
        const auto SS = (GXraw - 1.0)*(GXraw - 1.0)/std::log(2.0); //sigma^2.
        Iraw = std::exp(-(X-1.0)*(X-1.0)/SS);

    }else if(this->Schedule == dspln::sched::gaussian_em2mons){
        //Very similar to gaussian_em1day, but with the time shift altered.
        const auto lT = static_cast<double>(T);
        const auto GXraw = (lT - static_cast<double>(2*30*24*60*60))/lT;
        const auto SS = (GXraw - 1.0)*(GXraw - 1.0)/std::log(2.0); //sigma^2.
        Iraw = std::exp(-(X-1.0)*(X-1.0)/SS);

    }else if(this->Schedule == dspln::sched::gaussian_em2xestlen){
        //Gaussian schedule with the half-maximum at Tend - 2*T_{estimated length}.
        //
        //Note: This schedule is almost identical to 'gaussian_em1day', but with the
        // width of the Gaussian altered (1 day -> 2 x Estimated Length).
        if(this->Estimated_Length == 0) FUNCERR("Cannot safely compute schedule");
        const auto lT = static_cast<double>(T);
        const auto GXraw = (lT - 2.0*static_cast<double>(this->Estimated_Length))/lT;
        const auto SS = (GXraw - 1.0)*(GXraw - 1.0)/std::log(2.0); //sigma^2.
        Iraw = std::exp(-(X-1.0)*(X-1.0)/SS);

    }else if(this->Schedule == dspln::sched::gaussian_em20xestlen){
        //Very similar to gaussian_em2xestlen, but with the time shoft altered.
        if(this->Estimated_Length == 0) FUNCERR("Cannot safely compute schedule");
        const auto lT = static_cast<double>(T);
        const auto GXraw = (lT - 20.0*static_cast<double>(this->Estimated_Length))/lT;
        const auto SS = (GXraw - 1.0)*(GXraw - 1.0)/std::log(2.0); //sigma^2.
        Iraw = std::exp(-(X-1.0)*(X-1.0)/SS);

    }else if(this->Schedule == dspln::sched::gaussian_em200xestlen){
        //Very similar to gaussian_em2xestlen, but with the time shoft altered.
        if(this->Estimated_Length == 0) FUNCERR("Cannot safely compute schedule");
        const auto lT = static_cast<double>(T);
        const auto GXraw = (lT - 200.0*static_cast<double>(this->Estimated_Length))/lT;
        const auto SS = (GXraw - 1.0)*(GXraw - 1.0)/std::log(2.0); //sigma^2.
        Iraw = std::exp(-(X-1.0)*(X-1.0)/SS);

    }else{
        FUNCWARN("Schedule type not recognized. Cannot determine priority");
        return this->If; //Safest to fall back on final priority. Let the human sort out any issues!
    }
    return this->Ii + dI*Iraw; //Scale the result.
}

double Deadline::Get_Current_Priority() const {
    time_mark tnow;
    return this->Get_Priority_At(tnow);
}

//This routine will check that in the specified block of time (A - B) the task
// was being worked on. It checks that there is (a) a block of Working time that
// bounds both the earlier and later times.
//
//Note: this routine doesn't care if the task is/was open, closed, or whatever!
// Check the completion/begin date if this info is needed.
//
//Note: this function will project into the future, assuming the last state
// continue(s/d) forever.
//
//Note: this function doesn't care about the order of A and B. 
//
//Note: this function CAN handle the case that A == B. But probably cannot 
// properly handle the case with multiple equiv Working timestamps (due to not
// knowing how std::map will order the degenerate timestamps).
//
bool Deadline::Was_Being_Worked_On_Between_Inc(const time_mark &tAin, const time_mark &tBin) const {
    const auto tA = ((tAin < tBin) ? tAin : tBin); //Ensure tA <= tB.
    const auto tB = ((tAin < tBin) ? tBin : tAin);

    //Given a timespan A=====B and the Working timeline
    // ...-----X=====X----X===========X----X======X--------.....
    // we want to see if A=====B can fit inclusively into any of 
    // the X==...==X blocks.

    //Simple cases.
    if(this->Working.empty()) return false; //Empty timeline.
    if(tB < this->Working.begin()->first) return false;
    if(this->Working.rbegin()->first < tA){
        return (this->Working.rbegin()->second == dspln::working::began);
    }

    //Find the two consecutive times that bound both tA and tB.
    auto it1 = this->Working.begin();
    auto it2 = std::next(it1);
    for( ; it2 != this->Working.end(); ++it1, ++it2){
        if( isininc(it1->first, tA, it2->first)
        &&  isininc(it1->first, tB, it2->first)
        &&  (it1->second == dspln::working::began)
        &&  (it2->second == dspln::working::ended) ){
            return true;
        }
    }

    return false;
}

bool Deadline::Was_Being_Worked_On_At(const time_mark &t) const {
    return this->Was_Being_Worked_On_Between_Inc(t,t);    
}

bool Deadline::Is_Curr_Worked_On() const {
    time_mark now;
    return this->Was_Being_Worked_On_At(now);
}

//Returns all Working timestamps which occur within the gap A - B.
//
//Note: A and B do not need to be ordered. They are sorted so tA <= tB.
//
//Note: If a Working began/ended pair encompass A - B, then NO times are 
// returned. The Working timestamps need to occur within [tA,tB].
std::map<time_mark,uint64_t> Deadline::Working_Between_Inc(const time_mark &tAin, const time_mark &tBin) const {
    const auto tA = (tAin < tBin) ? tAin : tBin; //Ensure tA <= tB.
    const auto tB = (tAin < tBin) ? tBin : tAin;
    std::map<time_mark,uint64_t> out;
    for(auto m_it = this->Working.begin(); m_it != this->Working.end(); ++m_it){
        if(isininc(tA,m_it->first,tB)) out.insert(*m_it);
    }
    return out;
}


//Returns the sum of timespans (in minutes) between 'work began' and 'work ended'.
//
//Note: Returns a -1.0 on error.
//Note: This function will ignore timespans in the future (i.e., after tspec).
//Note: This function will interpolate a timespan if the provided time occurs within it.
//Note: If the last timestamp was a 'work began', and the provided time is afterward, 
//      then the time between will contribute to the total work time.
double Deadline::Get_Work_Time_At(const time_mark &tspec) const {
    if(this->Working.empty()) return 0.0;
    if(!this->Verify_Consistency()){
        FUNCWARN("Failed consistency check. Not attempting to compute work time");
        return -1.0;
    }

    double out(0.0);
    auto ita = this->Working.begin(); //Should always be non-end().
    auto itb = std::next(ita); //Might be end().
    while(ita != this->Working.end()){
        if(ita->second != dspln::working::began){
            FUNCWARN("Work times could not be computed. Expected a BEGAN");
            return -1.0;
        }else if((itb != this->Working.end()) && (itb->second != dspln::working::ended)){
            FUNCWARN("Work times could not be computed. Expected a BEGAN");
            return -1.0;
        }

        //Each of ta and tb will be one of {valid, invalid} and {in past, in future}.
        // If invalid, it won't matter if in future or past.

        //Case: Both are valid, both ta and tb are in the past.
        if((itb != this->Working.end()) && (ita->first < tspec) && (itb->first < tspec)){
            out += static_cast<double>(ita->first.Diff_in_Seconds(itb->first))/60.0;

        //Case: Both are valid, ta is in the past but tb is in future.
        //Case: Only ta is valid and is in the past.
        }else if(ita->first < tspec){
            out += static_cast<double>(ita->first.Diff_in_Seconds(tspec))/60.0;

        }

        //Irrelevant cases:
        //Case: Both are valid, but tb occurs before ta.  ---> not possible (sorted).
        //Case: Both are valid, both are in the future.  ---> no contrib.
        //Case: Only ta is valid and is in future.  ---> no contrib.
        //Case: Both are not valid. ---> not possible (ta is checked each iteration).

        if(itb == this->Working.end()) break; //It is the last one, no need to advance ta...
        ita = std::next(itb); //If this is end(), no reason to continue.
        if(ita == this->Working.end()) break;
        itb = std::next(ita); //Might be end().
    }
    return out;
}

double Deadline::Get_Current_Work_Time() const {
    time_mark tnow;
    return this->Get_Work_Time_At(tnow);
}

//Returns the number of minutes between A and B (or B and A) where work was 
// being done. This routine should properly handle the case of a currently-
// being-worked-on item (possibly up to a few seconds due to computation). 
//
//Note: Returns -1.0 on error, else number of minutes.
double Deadline::Get_Total_Working_Time_Between(const time_mark &tAin, const time_mark &tBin) const {
    const auto tA = (tAin < tBin) ? tAin : tBin; //Ensure tA <= tB.
    const auto tB = (tAin < tBin) ? tBin : tAin;
    double out(0.0);

    //Given a timespan A=====B and the Working timeline
    // ...-----X=====X----X===========X----X======X--------.....
    // we want to count the number of minutes where they overlap. 

    //Empty timeline or interval cases.
    if(this->Working.empty()) return out;
    if(tA == tB) return out;

    //No overlap cases.
    if(tB < this->Working.begin()->first) return out;
    if((this->Working.rbegin()->first < tA)
    && (this->Working.rbegin()->second == dspln::working::ended)) return out;

    //Compute all the work time leading up to A.
    const auto dtA = this->Get_Work_Time_At(tA);
    const auto dtB = this->Get_Work_Time_At(tB);
    if((dtA < 0.0) || (dtB < 0.0)) return -1.0;
    const auto dt = ((dtB - dtA) < 0.0) ? 0.0 : (dtB - dtA); //Ensure non-neg (roundoff...)
    return dt;
}

double Deadline::Get_Total_Working_Time_On_Day(const time_mark &tspec) const {
    const time_mark tA = tspec.Same_Day_Earliest(); //Earliest time that day.
    const time_mark tB = tspec.Same_Day_Latest();   //Latest time that day.
    const time_mark t; //Current date and time.
    const time_mark tBp = ( (tB > t) ? t : tB ); //Do not extrapolate into the future.
    return this->Get_Total_Working_Time_Between(tA,tBp);
}


void Deadline::Mark_Active(){
    this->Status = dspln::status::active;
    this->Completed = time_mark().One_Day_Earlier_Than( this->Begin );
    return;
}
void Deadline::Mark_Completed(){
    this->Status = dspln::status::completed;
    this->Completed = time_mark(); //Guaranteed to be current time.
    return;
}


void Deadline::Mark_Work_Began_At(const time_mark &t){
    this->Working[t] = dspln::working::began;
    return;
}
void Deadline::Mark_Work_Ended_At(const time_mark &t){
    this->Working[t] = dspln::working::ended;
    return;
}
void Deadline::Mark_Work_Began(){
    time_mark now;
    return this->Mark_Work_Began_At(now);
}
void Deadline::Mark_Work_Ended(){
    time_mark now;
    return this->Mark_Work_Ended_At(now);
}

//Check that the data appears to be consistent. Put any requirements on the members here.
bool Deadline::Verify_Consistency() const {
    //Basic status verification.
    if( (this->Status != dspln::status::active) 
    &&  (this->Status != dspln::status::completed) 
    &&  (this->Status != dspln::status::abandoned) ){
        FUNCWARN("Status is impossible");
        return false;
    }

    //Basic schedule verification.
    if( (this->Schedule != dspln::sched::linear) 
    &&  (this->Schedule != dspln::sched::sigmoidal) 
    &&  (this->Schedule != dspln::sched::gaussian_em1day) 
    &&  (this->Schedule != dspln::sched::gaussian_em3days) 
    &&  (this->Schedule != dspln::sched::gaussian_em1week) 
    &&  (this->Schedule != dspln::sched::gaussian_em2weeks) 
    &&  (this->Schedule != dspln::sched::gaussian_em1mon) 
    &&  (this->Schedule != dspln::sched::gaussian_em2mons) 
    &&  (this->Schedule != dspln::sched::gaussian_em2xestlen)
    &&  (this->Schedule != dspln::sched::gaussian_em20xestlen) 
    &&  (this->Schedule != dspln::sched::gaussian_em200xestlen) ){
        FUNCWARN("Schedule is impossible");
        return false;
    }

    //Basic timing verification.
    if(this->End < this->Begin){
        FUNCWARN("End date/time is before Begin date/time");
        return false;
    }

    //Basic importance verification.
    if((this->Ii < 0.0) || (this->Ii > 1.0)){
        FUNCWARN("Initial importance is out of bounds");
        return false;
    }
    if((this->If < 0.0) || (this->If > 1.0)){
        FUNCWARN("Final importance is out of bounds");
        return false;
    }

    //Basic UID verification.
    if(this->UID.empty()) return false;

    //Iff completed, check that the completion time is after the Begin time.
    if((this->Status == dspln::status::completed) 
    && (this->Completed < this->Begin) ){
        FUNCWARN("Completed task's completion date/time is before Begin date/time");
        return false;
    }

    //Check that the working times alternate. Do NOT check their relation to the
    // begin/end times.
    if(!this->Working.empty()){
        bool Flipper = true; //true == 'should be a begin', false == 'should be an end'
        for(auto it = this->Working.begin(); it != this->Working.end(); ++it){
            if(Flipper == true){
                if(it->second != dspln::working::began){
                    FUNCWARN("Expected working timestamp to be BEGAN");
                    return false;
                }
            }else if(Flipper == false){
                if(it->second != dspln::working::ended){ 
                    FUNCWARN("Expected working timestamp to be ENDED");
                    return false;
                }
            }
            Flipper = !Flipper;
        }
    }

    //I'm not sure whether to consider Working times AFTER the completion date
    // (iff completed) to be valid or not. Often, a little thing here or there are
    // required for tasks (e.g., follow-up emails, clarifying something, etc..)
    // I think it would be BETTER to allow them so that I can explicitly track
    // such 'extra' work. Likewise for begin times.


    return true;
}


bool Deadline::Read_From_File(const std::string &filenamein){
    auto inmem = LoadFileToList(filenamein);

    //Clean up the file and whine if nothing remains.
    {
      auto l_it = inmem.begin();
      while(l_it != inmem.end()){
          const std::string cleaned = Canonicalize_String2(*l_it, CANONICALIZE::TRIM);
//          if(cleaned.empty()){  //Blank or whitespace-only line: remove it.
//              l_it = inmem.erase(l_it);
//          }else 
          if(cleaned.substr(0,1) == "#"){  //Comment line. Ignore it.  (Can I somehow keep these? Is there a valid reason to keep them?)
              l_it = inmem.erase(l_it);
          }else{  //Probably a non-comment, non-blank line. Keep it.
              ++l_it;
          }
      }
    }
    if(inmem.empty()){
        FUNCWARN("File '" << filenamein << "' either didn't exist or contained no text");
        return false;
    }

    //Parse the file.
    const unsigned char uid_mask  = CANONICALIZE::TRIM_ALL;
    const unsigned char line_mask = CANONICALIZE::TRIM_ALL | CANONICALIZE::TO_UPPER; //The easiest/safest way to compare keys/lines in a whitespace-indep.
    const unsigned char key_mask  = CANONICALIZE::TRIM_ALL | CANONICALIZE::TO_UPPER; // way is to trim all space. If you want whitespace to be significant
    const unsigned char shtl_mask = CANONICALIZE::TRIM_ALL | CANONICALIZE::TO_UPPER; // then alter the TRIM_ALL's to TRIM and go from there.

    bool in_tags = false, in_title = false, in_descrip = false, in_notes = false;
    bool had_uid = false, had_tags = false, had_title = false, had_descrip = false, had_begin = false, had_end = false, had_status = false, had_schedule = false;
    std::string key;
    int Version = -1;
    std::string shtl;
    time_mark time_buff;
    for(auto l_it = inmem.begin(); l_it != inmem.end(); ++l_it){

        //The "modal" fields.
        //
        //We want to keep blank/empty lines for most of these fields, even if the text entry
        // portions disallow it. Why? It is nice for long Notes/Descriptions to keep whitespace
        // significant.


        key = "</Tags>";
        if(Safely_Compare_Strings(*l_it,0,key.size()-1,line_mask, key,0,key.size()-1,key_mask)){
            if(!in_tags){ FUNCWARN("Missing a <Tags> tag! Bailing"); return false; }
            in_tags = false; continue;
        }
        key = "</Title>";
        if(Safely_Compare_Strings(*l_it,0,key.size()-1,line_mask, key,0,key.size()-1,key_mask)){
            if(!in_title){ FUNCWARN("Missing a <Title> tag! Bailing"); return false; }
            in_title = false; continue;
        }
        key = "</Description>";
        if(Safely_Compare_Strings(*l_it,0,key.size()-1,line_mask, key,0,key.size()-1,key_mask)){
            if(!in_descrip){ FUNCWARN("Missing a <Description> tag! Bailing"); return false; }
            in_descrip = false; continue;
        }
        key = "</Notes>";
        if(Safely_Compare_Strings(*l_it,0,key.size()-1,line_mask, key,0,key.size()-1,key_mask)){
            if(!in_notes){ FUNCWARN("Missing a <Notes> tag! Bailing"); return false; }
            in_notes = false; continue;
        }

        key = "<Tags>";
        if(Safely_Compare_Strings(*l_it,0,key.size()-1,line_mask, key,0,key.size()-1,key_mask)){
            if(in_tags || in_title || in_descrip || in_notes){ FUNCWARN("Two opened tags! Bailing"); return false; }
            this->Tags.clear();
            in_tags = true; continue;
        }
        key = "<Title>";
        if(Safely_Compare_Strings(*l_it,0,key.size()-1,line_mask, key,0,key.size()-1,key_mask)){
            if(in_tags || in_title || in_descrip || in_notes){ FUNCWARN("Two opened tags! Bailing"); return false; }
            this->Title.clear();
            in_title = true; continue;
        }
        key = "<Description>";
        if(Safely_Compare_Strings(*l_it,0,key.size()-1,line_mask, key,0,key.size()-1,key_mask)){
            if(in_tags || in_title || in_descrip || in_notes){ FUNCWARN("Two opened tags! Bailing"); return false; }            
            this->Description.clear();
            in_descrip = true; continue;
        }
        key = "<Notes>";
        if(Safely_Compare_Strings(*l_it,0,key.size()-1,line_mask, key,0,key.size()-1,key_mask)){
            if(in_tags || in_title || in_descrip || in_notes){ FUNCWARN("Two opened tags! Bailing"); return false; }            
            this->Notes.clear();
            in_notes = true; continue;
        }

        if(in_tags){
            had_tags = true;
            if(this->Tags.empty()){ this->Tags += *l_it;
            }else{                  this->Tags += "\n" + *l_it;
            }
            continue;
        }else if(in_title){
            had_title = true;
            if(this->Title.empty()){ this->Title += *l_it;
            }else{                   this->Title += "\n" + *l_it;
            }
            continue;
        }else if(in_descrip){
            had_descrip = true;
            if(this->Description.empty()){ this->Description += *l_it;
            }else{                         this->Description += "\n" + *l_it;
            }
            continue;
        }else if(in_notes){
            if(this->Notes.empty()){ this->Notes += *l_it;
            }else{                   this->Notes += "\n" + *l_it;
            }
            continue;
        }


        //The "non-modal" (i.e., value-specified) fields.
        //
        //Blank/empty lines are not important here. If this line is blank, simply ignore it.
        if(Canonicalize_String2(*l_it, CANONICALIZE::TRIM).empty()) continue;


        if(GetValueIfKeyMatches(&Version,"Version = ",key_mask, *l_it, line_mask)){
            if(Version != 1){
                FUNCWARN("Parsed a file with an un-recognized version number. Bailing");
                return false;
            }
        }else if(GetValueIfKeyMatches(&shtl,"Status = ",key_mask, *l_it, line_mask) 
        &&      (Canonicalize_String2("active",shtl_mask) == shtl)){
            this->Status = dspln::status::active;      had_status = true;
        }else if(GetValueIfKeyMatches(&shtl,"Status = ",key_mask, *l_it, line_mask)
        &&      (Canonicalize_String2("completed",shtl_mask) == shtl)){
            this->Status = dspln::status::completed;   had_status = true;
        }else if(GetValueIfKeyMatches(&shtl,"Status = ",key_mask, *l_it, line_mask) 
        &&      (Canonicalize_String2("abandoned",shtl_mask) == shtl)){
            this->Status = dspln::status::abandoned;   had_status = true;

        }else if(GetValueIfKeyMatches(&shtl,"Schedule = ",key_mask, *l_it, line_mask)
        &&      (Canonicalize_String2("linear",shtl_mask) == shtl)){
            this->Schedule = dspln::sched::linear;                had_schedule = true;
        }else if(GetValueIfKeyMatches(&shtl,"Schedule = ",key_mask, *l_it, line_mask)
        &&      (Canonicalize_String2("sigmoidal",shtl_mask) == shtl)){
            this->Schedule = dspln::sched::sigmoidal;             had_schedule = true;

        }else if(GetValueIfKeyMatches(&shtl,"Schedule = ",key_mask, *l_it, line_mask)
        &&      (Canonicalize_String2("gaussian_em1day",shtl_mask) == shtl)){
            this->Schedule = dspln::sched::gaussian_em1day;       had_schedule = true;
        }else if(GetValueIfKeyMatches(&shtl,"Schedule = ",key_mask, *l_it, line_mask)
        &&      (Canonicalize_String2("gaussian_em3days",shtl_mask) == shtl)){
            this->Schedule = dspln::sched::gaussian_em3days;      had_schedule = true;
        }else if(GetValueIfKeyMatches(&shtl,"Schedule = ",key_mask, *l_it, line_mask)
        &&      (Canonicalize_String2("gaussian_em1week",shtl_mask) == shtl)){
            this->Schedule = dspln::sched::gaussian_em1week;      had_schedule = true;
        }else if(GetValueIfKeyMatches(&shtl,"Schedule = ",key_mask, *l_it, line_mask)
        &&      (Canonicalize_String2("gaussian_em2weeks",shtl_mask) == shtl)){
            this->Schedule = dspln::sched::gaussian_em2weeks;     had_schedule = true;
        }else if(GetValueIfKeyMatches(&shtl,"Schedule = ",key_mask, *l_it, line_mask)
        &&      (Canonicalize_String2("gaussian_em1mon",shtl_mask) == shtl)){
            this->Schedule = dspln::sched::gaussian_em1mon;      had_schedule = true;
        }else if(GetValueIfKeyMatches(&shtl,"Schedule = ",key_mask, *l_it, line_mask)
        &&      (Canonicalize_String2("gaussian_em2mons",shtl_mask) == shtl)){
            this->Schedule = dspln::sched::gaussian_em2mons;     had_schedule = true;


        }else if(GetValueIfKeyMatches(&shtl,"Schedule = ",key_mask, *l_it, line_mask) 
        &&      (Canonicalize_String2("gaussian_em2xestlen",shtl_mask) == shtl)){
            this->Schedule = dspln::sched::gaussian_em2xestlen;   had_schedule = true;
        }else if(GetValueIfKeyMatches(&shtl,"Schedule = ",key_mask, *l_it, line_mask)
        &&      (Canonicalize_String2("gaussian_em20xestlen",shtl_mask) == shtl)){
            this->Schedule = dspln::sched::gaussian_em20xestlen;   had_schedule = true;
        }else if(GetValueIfKeyMatches(&shtl,"Schedule = ",key_mask, *l_it, line_mask)
        &&      (Canonicalize_String2("gaussian_em200xestlen",shtl_mask) == shtl)){
            this->Schedule = dspln::sched::gaussian_em200xestlen;   had_schedule = true;

        }else if(GetValueIfKeyMatches(&(this->Estimated_Length),"Estimated Length = ",key_mask, *l_it, line_mask)){
        }else if(GetValueIfKeyMatches(&(this->A),"A = ",key_mask, *l_it, line_mask)){
        }else if(GetValueIfKeyMatches(&(this->B),"B = ",key_mask, *l_it, line_mask)){
        }else if(GetValueIfKeyMatches(&(this->C),"C = ",key_mask, *l_it, line_mask)){
        }else if(GetValueIfKeyMatches(&(this->D),"D = ",key_mask, *l_it, line_mask)){
        }else if(GetValueIfKeyMatches(&(this->Ii),"Initial Priority = ",key_mask, *l_it, line_mask)){
        }else if(GetValueIfKeyMatches(&(this->If),"Final Priority = ",key_mask, *l_it, line_mask)){
        }else if(GetValueIfKeyMatches(&shtl,"Begins = ",key_mask, *l_it, line_mask) && this->Begin.Read_from_string(shtl)){
            had_begin = true;
        }else if(GetValueIfKeyMatches(&shtl,"Ends = ",key_mask, *l_it, line_mask) && this->End.Read_from_string(shtl)){
            had_end = true;
        }else if(GetValueIfKeyMatches(&shtl,"Completed = ",key_mask, *l_it, line_mask) && this->Completed.Read_from_string(shtl)){
        }else if(GetValueIfKeyMatches(&shtl,"Work Began = ",key_mask, *l_it, line_mask) && time_buff.Read_from_string(shtl)){
            this->Working[time_buff] = dspln::working::began;
        }else if(GetValueIfKeyMatches(&shtl,"Work Ended = ",key_mask, *l_it, line_mask) && time_buff.Read_from_string(shtl)){
            this->Working[time_buff] = dspln::working::ended;
        }else if(GetValueIfKeyMatches(&(this->UID),"UID = ",key_mask, *l_it, uid_mask)){
            had_uid = true;
        }else{
            FUNCWARN("Did not recognize the line '" << Convert_Unprintables_to_Hex(*l_it) << "'. Ignoring it");
        }
    }

    if(in_tags || in_title || in_descrip || in_notes){
        FUNCWARN("File '" << filenamein << "' has unclosed tags. Bailing");
        return false;
    }else if( !had_uid || !had_title || !had_descrip || !had_begin || !had_end || !had_status || !had_schedule ){
        FUNCWARN("File '" << filenamein << "' was missing required information. Bailing");
        return false;
    }

    this->Filename_Origin = filenamein;

    if(!this->Verify_Consistency()){
        FUNCWARN("File '" << filenamein << "' failed consistency verification");
        return false;
    }

    return true;
}

bool Deadline::Write_To_File(const std::string &filenameout) const {
    std::stringstream ss;
    time_mark now;
    ss << "# Generated on " << now.Dump_as_string() << std::endl;
    ss << "Version = " << 1 << std::endl;

    ss << std::endl;
    if(this->Status == dspln::status::active){            ss << "Status = active" << std::endl;
    }else if(this->Status == dspln::status::completed){   ss << "Status = completed" << std::endl;
    }else if(this->Status == dspln::status::abandoned){   ss << "Status = abandoned" << std::endl;
    }else{
        FUNCWARN("Unrecognized value stored in this->Status");
        return false;
    }

    ss << std::endl;
    if(this->Schedule == dspln::sched::linear){                      ss << "Schedule = linear" << std::endl;
    }else if(this->Schedule == dspln::sched::sigmoidal){             ss << "Schedule = sigmoidal" << std::endl;

    }else if(this->Schedule == dspln::sched::gaussian_em1day){       ss << "Schedule = gaussian_em1day" << std::endl;
    }else if(this->Schedule == dspln::sched::gaussian_em3days){      ss << "Schedule = gaussian_em3days" << std::endl;
    }else if(this->Schedule == dspln::sched::gaussian_em1week){      ss << "Schedule = gaussian_em1week" << std::endl;
    }else if(this->Schedule == dspln::sched::gaussian_em2weeks){     ss << "Schedule = gaussian_em2weeks" << std::endl;
    }else if(this->Schedule == dspln::sched::gaussian_em1mon){       ss << "Schedule = gaussian_em1mon" << std::endl;
    }else if(this->Schedule == dspln::sched::gaussian_em2mons){      ss << "Schedule = gaussian_em2mons" << std::endl;

    }else if(this->Schedule == dspln::sched::gaussian_em2xestlen){   ss << "Schedule = gaussian_em2xestlen" << std::endl;
    }else if(this->Schedule == dspln::sched::gaussian_em20xestlen){  ss << "Schedule = gaussian_em20xestlen" << std::endl;
    }else if(this->Schedule == dspln::sched::gaussian_em200xestlen){ ss << "Schedule = gaussian_em200xestlen" << std::endl;
    }else{
        FUNCWARN("Unrecognized value stored in this->Schedule");
        return false;
    }

    ss << std::endl;
    ss << "Estimated Length = " << this->Estimated_Length << std::endl;
    ss << std::endl;
    for(auto it = this->Working.begin(); it != this->Working.end(); ++it){
        if(it->second == dspln::working::began){         ss << "Work Began = " << it->first.Dump_as_string() << std::endl;
        }else if(it->second == dspln::working::ended){   ss << "Work Ended = " << it->first.Dump_as_string() << std::endl;
        }else{
            FUNCWARN("Unrecognized value stored in this->Working");
            return false;
        }
    }
    ss << std::endl;
    ss << "A = " << this->A << std::endl;
    ss << "B = " << this->B << std::endl;
    ss << "C = " << this->C << std::endl;
    ss << "D = " << this->D << std::endl;
    ss << std::endl;
    ss << "Initial Priority = " << this->Ii << std::endl;
    ss << "Final Priority = " << this->If << std::endl;
    ss << std::endl;
    ss << "Begins = " << this->Begin.Dump_as_string() << std::endl;
    ss << "Ends = " << this->End.Dump_as_string() << std::endl;
    ss << "Completed = " << this->Completed.Dump_as_string() << std::endl;
    ss << std::endl;
    ss << "UID = " << this->UID << std::endl;
    ss << std::endl;
    ss << "<Title>" << std::endl;
    ss << this->Title << std::endl;
    ss << "</Title>" << std::endl;
    ss << std::endl;
    ss << "<Description>" << std::endl;
    ss << this->Description << std::endl;
    ss << "</Description>" << std::endl;
    ss << std::endl;
    ss << "<Notes>" << std::endl;
    ss << this->Notes << std::endl;
    ss << "</Notes>" << std::endl;
    ss << std::endl;
    ss << "<Tags>" << std::endl;
    ss << this->Tags << std::endl;
    ss << "</Tags>" << std::endl;
    ss << std::endl;
    return WriteStringToFile(ss.str(), filenameout, true); //Force an overwrite if required.
}


//Serialize (deeply) to buffer starting at *offset. Buffer can be nullptr or passed in, but 
// other params must be appropriately set (if applicable) and always NOT-nullptr. On 
// failure, *OK set to false.
//
// Input:
//
//   - *OK       - Optional - Used to indicate success or failure. In absence, program is 
//                             terminated on failure.
//
//   - *in       - Optional - If provided, serialization is deposited into buffer (starting
//                             at *offset), *offset is advanced to the back of the 
//                             serialized data, and *buf_size is treated as the maximum 
//                             size of the buffer (and is not altered).
//                             If not provided, an appropriate amount of space will be 
//                             allocated using the default allocator prior to serialization.
//
//   - *offset   - Required - Used by the user to tell where to begin serialization (often
//                             at 0) and by this routine to tell the user where the next
//                             available byte is after serialization is complete.
//                             If "in" buffer is nullptr, *offset is set to 0 prior to
//                             serialization.
//
//   - *buf_size - Required - If "in" buffer is non-nullptr, this must hold the number of
//                             allocated bytes, and it will not be altered. 
//                             If "in" buffer is nullptr, this will afterward hold the 
//                             number of allocated bytes (which may be >= the amount of 
//                             data serialized).
//
//NOTE: The buffer may be larger than the serialized content, thus allowing many serialized 
// instances in one large buffer. The semantics of this are up to the user.
std::unique_ptr<uint8_t[]>
Deadline::Serialize(bool *OK, std::unique_ptr<uint8_t[]> in, uint64_t *offset, uint64_t *buf_size) const {
    if(OK != nullptr) *OK = false;
    SERIALIZE_WARNFAIL_OR_DIE((buf_size == nullptr), OK, "Passed an invalid *buf_size.",std::move(in));
    SERIALIZE_WARNFAIL_OR_DIE((offset == nullptr), OK, "Passed an invalid *offset.",std::move(in));
    SERIALIZE_WARNFAIL_OR_DIE(!this->Verify_Consistency(), OK, "Failed consistency check.",std::move(in));

FUNCERR("This is currently out of date and eligible for removal. For example, this->Working is not included here");

    //If the buffer is not yet allocated, allocate an appropriately-sized buffer.
    if(in == nullptr){
        *buf_size = this->Theo_Max_Serialization_Size();
        in.reset( new uint8_t[ *buf_size ] );
        *offset = 0;
    }

    bool l_OK;

    //------ <Version 1 @ 20140114> -------
    //Verify that the double representation is standard.
    l_OK = (std::numeric_limits<double>::is_iec559 &&
           (std::numeric_limits<double>::max_exponent == 1024) &&
           (std::numeric_limits<double>::digits == 53) &&
           (std::numeric_limits<double>::radix == 2));
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Non-standard platform double representation.",std::move(in));

    //Version number.
    const uint64_t version = 1;
    in = SERIALIZE::Put_Size(&l_OK,std::move(in),offset,*buf_size, version);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put version.",std::move(in));

    //Status and Schedule (as variable-width sizes).
    in = SERIALIZE::Put_Size(&l_OK,std::move(in),offset,*buf_size, this->Status);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put this->Status.",std::move(in));
    in = SERIALIZE::Put_Size(&l_OK,std::move(in),offset,*buf_size, this->Schedule);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put this->Schedule.",std::move(in));

    //Estimated_Length (as variable-width size).
    in = SERIALIZE::Put_Size(&l_OK,std::move(in),offset,*buf_size, this->Estimated_Length);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put this->Estimated_Length.",std::move(in));

    //A and B and C and D.
    in = SERIALIZE::Put(&l_OK,std::move(in),offset,*buf_size, this->A);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put this->A.",std::move(in));
    in = SERIALIZE::Put(&l_OK,std::move(in),offset,*buf_size, this->B);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put this->B.",std::move(in));
    in = SERIALIZE::Put(&l_OK,std::move(in),offset,*buf_size, this->C);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put this->C.",std::move(in));
    in = SERIALIZE::Put(&l_OK,std::move(in),offset,*buf_size, this->D);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put this->D.",std::move(in));

    //Ii and If.
    in = SERIALIZE::Put(&l_OK,std::move(in),offset,*buf_size, this->Ii);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put this->Ii.",std::move(in));
    in = SERIALIZE::Put(&l_OK,std::move(in),offset,*buf_size, this->If);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put this->If.",std::move(in));

    //Begin, End, and Completed (in std::string format).
    const auto stringified_begin = this->Begin.Dump_as_string();
    in = SERIALIZE::Put(&l_OK,std::move(in),offset,*buf_size, stringified_begin);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put stringified this->Begin.",std::move(in));
    const auto stringified_end = this->End.Dump_as_string();
    in = SERIALIZE::Put(&l_OK,std::move(in),offset,*buf_size, stringified_end);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put stringified this->End.",std::move(in));
    const auto stringified_completed = this->Completed.Dump_as_string();
    in = SERIALIZE::Put(&l_OK,std::move(in),offset,*buf_size, stringified_completed);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put stringified this->Completed.",std::move(in));

    //Title and Description (in std::string format).
    in = SERIALIZE::Put(&l_OK,std::move(in),offset,*buf_size, this->Title);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put stringified this->Title.",std::move(in));
    in = SERIALIZE::Put(&l_OK,std::move(in),offset,*buf_size, this->Description);
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Put stringified this->Description.",std::move(in));

    //Filename_Origin is not serialized.

    //------ </Version 1 @ 20140114> -------

    if(OK != nullptr) *OK = true;
    return std::move(in);
}

//Deserialize (deeply) from buffer starting at *offset. Contrary to the counterpart
// Serialize function, this function requires a valid buffer to deserialize from.
// Thus, all parameters (except *OK) are required.
//
//This routine will overwrite *this.
//
//NOTE: The buffer may be larger than the serialized content, thus allowing many 
// serialized instances in one large buffer. The semantics of this are up to the user.
// This routine will only deserialize the data beginning at *offset.
std::unique_ptr<uint8_t[]> 
Deadline::Deserialize(bool *OK, std::unique_ptr<uint8_t[]> in, uint64_t *offset, uint64_t buf_size){
    if(OK != nullptr) *OK = false;
    SERIALIZE_WARNFAIL_OR_DIE((offset == nullptr), OK, "Passed an invalid *offset.",std::move(in));

FUNCERR("This is currently out of date and eligible for removal. For example, this->Working is not included here");

    bool l_OK;
    uint64_t version;
    in = SERIALIZE::Get_Size(&l_OK,std::move(in),offset,buf_size, &(version));
    SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get version.",std::move(in));

    if(version == 1){
        //------ <Version 1 @ 20140114> -------
        //Verify that the double representation is standard.
        l_OK = (std::numeric_limits<double>::is_iec559 &&
               (std::numeric_limits<double>::max_exponent == 1024) &&
               (std::numeric_limits<double>::digits == 53) &&
               (std::numeric_limits<double>::radix == 2));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Non-standard platform double representation.",std::move(in));

        //Status and Schedule (as variable-width sizes).
        in = SERIALIZE::Get_Size(&l_OK,std::move(in),offset,buf_size, &(this->Status));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get this->Status.",std::move(in));
        in = SERIALIZE::Get_Size(&l_OK,std::move(in),offset,buf_size, &(this->Schedule));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get this->Schedule.",std::move(in));

        //Estimated_Length.
        in = SERIALIZE::Get_Size(&l_OK,std::move(in),offset,buf_size, &(this->Estimated_Length));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get this->Estimated_Length.",std::move(in));

        //A and B and C and D.
        in = SERIALIZE::Get(&l_OK,std::move(in),offset,buf_size, &(this->A));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get this->A.",std::move(in));
        in = SERIALIZE::Get(&l_OK,std::move(in),offset,buf_size, &(this->B));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get this->B.",std::move(in));
        in = SERIALIZE::Get(&l_OK,std::move(in),offset,buf_size, &(this->C));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get this->C.",std::move(in));
        in = SERIALIZE::Get(&l_OK,std::move(in),offset,buf_size, &(this->D));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get this->D.",std::move(in));

        //Ii and If.
        in = SERIALIZE::Get(&l_OK,std::move(in),offset,buf_size, &(this->Ii));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get this->Ii.",std::move(in));
        in = SERIALIZE::Get(&l_OK,std::move(in),offset,buf_size, &(this->If));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get this->If.",std::move(in));

        //Begin, End, and Completed (in std::string format).
        {
          std::string stringified_begin;
          in = SERIALIZE::Get(&l_OK,std::move(in),offset,buf_size, &stringified_begin);
          SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get stringified this->Begin.",std::move(in));
          l_OK = this->Begin.Read_from_string(stringified_begin);
          SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to interpret stringified this->Begin as valid date.",std::move(in));
        }
        {
          std::string stringified_end;
          in = SERIALIZE::Get(&l_OK,std::move(in),offset,buf_size, &stringified_end);
          SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get stringified this->End.",std::move(in));
          l_OK = this->End.Read_from_string(stringified_end);
          SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to interpret stringified this->End as valid date.",std::move(in));
        }
        {
          std::string stringified_completed;
          in = SERIALIZE::Get(&l_OK,std::move(in),offset,buf_size, &stringified_completed);
          SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get stringified this->Completed.",std::move(in));
          l_OK = this->Completed.Read_from_string(stringified_completed);
          SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to interpret stringified this->Completed as valid date.",std::move(in));
        }

        //Title and Description (in std::string format).
        in = SERIALIZE::Get(&l_OK,std::move(in),offset,buf_size, &(this->Title));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get stringified this->Title.",std::move(in));
        in = SERIALIZE::Get(&l_OK,std::move(in),offset,buf_size, &(this->Description));
        SERIALIZE_WARNFAIL_OR_DIE(!l_OK,OK,"Unable to Get stringified this->Description.",std::move(in));

        //Filename_Origin is not serialized.

        //------ </Version 1 @ 20140114> -------

    }else{
        SERIALIZE_WARNFAIL_OR_DIE(false,OK,"Version not recognized.",std::move(in));
    }

    //Verify that the deserialized data at least appears to be consistent.
    SERIALIZE_WARNFAIL_OR_DIE(!this->Verify_Consistency(), OK, "Failed consistency check.",std::move(in));

    if(OK != nullptr) *OK = true;
    return std::move(in);
}

//Returns the (maximum) number of bytes required to serialize *this (at this moment).
// Alteration of *any* of the members will probably void the estimate. Re-compute if 
// uncertain.
//
//This procedure depends strongly on the serialization procedure. Remember to keep it 
// up to date and versioned!
uint64_t Deadline::Theo_Max_Serialization_Size(void) const {
    uint64_t max_tot_size = 0;

FUNCERR("This routine is known to be out of date. It is flagged for removal");
    //------ <Version 1.0 @ 20140114> -------
    max_tot_size += SERIALIZE::max_vw_size; //Version stamp. 

    max_tot_size += 2*SERIALIZE::max_vw_size; //Status and Schedule.
    max_tot_size += 1*SERIALIZE::max_vw_size; //Estimated_Length.

    max_tot_size += 4*sizeof(this->A); //A and B and C and D.
    max_tot_size += 2*sizeof(this->Ii); //Ii and If.

    max_tot_size += this->Begin.Theo_Max_Serialization_Size(); 
    max_tot_size += this->End.Theo_Max_Serialization_Size(); 
    max_tot_size += this->Completed.Theo_Max_Serialization_Size(); 

    max_tot_size += SERIALIZE::max_string_head_size + this->Title.size(); 
    max_tot_size += SERIALIZE::max_string_head_size + this->Description.size();
    //------ </Version 1.0 @ 20140114> -------

    return max_tot_size;
}



bool Deadline_Sort_Compare_Higher_Priority_First(const Deadline &A, const Deadline &B){
    const auto Ap = A.Get_Current_Priority();
    const auto Bp = B.Get_Current_Priority();
    return Ap > Bp;
}

bool Deadline_Sort_Compare_Older_First(const Deadline &A, const Deadline &B){
    return A.End < B.End;
}

bool Deadline_Sort_Compare_Short_First(const Deadline &A, const Deadline &B){
    return A.Estimated_Length < B.Estimated_Length;
}

bool Deadline_Sort_Compare_Highest_Importance_Derivative_First(const Deadline &A, const Deadline &B){
    //Estimate the derivative of the current importance by finite differences.
    time_mark t; //Current time.
    auto t_b = t.Less_By_Seconds(60);
    auto t_f = t.More_By_Seconds(60);

    const auto dA = A.Get_Priority_At(t_f) - A.Get_Priority_At(t_b);
    const auto dB = B.Get_Priority_At(t_f) - B.Get_Priority_At(t_b);

    return dA > dB;
}

bool Deadline_Sort_Compare_Closest_To_Deadline_First(const Deadline &A, const Deadline &B){
    //Make the tasks temporally nearest to their deadline come first.
    // NOTE: both before and after deadline are considered. Pre-filter if this is not suitable.
    time_mark t; //Current time.
    return std::abs(t.Diff_in_Seconds(A.End)) < std::abs(t.Diff_in_Seconds(B.End));
}

bool Deadline_Sort_Compare_Most_Recently_Worked_On_First(const Deadline &A, const Deadline &B){
    //If a task has not ever been worked on, it is treated as if it were last worked on as far back in time as possible.
    // This routine also handles cases involving presently being worked on tasks.

    time_mark t; //Current time.
    time_t dTA;
    time_t dTB;

    if(A.Working.empty()){
        dTA = std::numeric_limits<time_t>::max();
    }else if(A.Is_Curr_Worked_On()){
        dTA = static_cast<time_t>(0);
    }else{
        dTA = std::abs(t.Diff_in_Seconds( A.Working.rbegin()->first ));
    }

    if(B.Working.empty()){
        dTB = std::numeric_limits<time_t>::max();
    }else if(B.Is_Curr_Worked_On()){
        dTB = static_cast<time_t>(0);
    }else{
        dTB = std::abs(t.Diff_in_Seconds( B.Working.rbegin()->first ));
    }

    return dTA < dTB;
}



