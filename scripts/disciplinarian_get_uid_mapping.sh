#!/usr/bin/env bash

# This script tries to find the full pathname of a task given the UID.
theuid="$1"
basedir="$2"
theuid=$(printf -- '%s' "$theuid" | tr -c -d '[:alnum:]')

set -e
pushd . &>/dev/null
cd "$basedir"

fname=$(grep -m 1 "^$theuid " UID_Ref_Mapping 2>/dev/null | cut -d ' ' -f 2  2>/dev/null)
[ -f "$fname" ] && realpath "$fname" | tr -d '\n'

popd &>/dev/null

