#!/usr/bin/env bash

basedir="$1"

set -e
shopt -s nullglob

pushd . &>/dev/null
cd "$basedir"

for i in ./{Active,Inactive}/*txt ; do 
    printf '%s %s\n' "$(grep -m 1 '^UID = DT' ${i} | 
      head -n 1 | 
      sed -e 's/UID = //')" "$i"
done > ./UID_Ref_Mapping

popd &>/dev/null

