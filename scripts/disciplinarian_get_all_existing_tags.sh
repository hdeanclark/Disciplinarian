#!/usr/bin/env bash

basedir="$1"

if [ ! -d "$basedir" ] ; then
    printf "Unable to access '$basedir'. Cannot continue.\n"
fi

set -e
shopt -s nullglob

for i in "$basedir"/{Active,Inactive}/*txt ; do 
    cat "$i" | 
      awk '/<Tags>/{f=1; next} /<\/Tags>/{f=0} f' | 
      sed -e 's/^\s*//g' -e 's/\s*$//g' -e '/^$/d'
done | 
  sort | 
  uniq -c | 
  sort --numeric-sort --reverse | 
  column -c 160


