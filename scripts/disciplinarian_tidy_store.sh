#!/usr/bin/env bash

basedir="$1"

set -e

pushd . &>/dev/null
cd "$basedir"

set +e
shopt -s nullglob

printf "Number of tasks in Active/ before cleanup ---> %d\n" $(ls Active/*txt 2>/dev/null | wc -l)

for i in Active/*txt ; do 
    iscompleted=$(grep -i 'Status = completed' "$i" 2>/dev/null) 
    isabandoned=$(grep -i 'Status = abandoned' "$i" 2>/dev/null)

    if [ "$iscompleted" != ""  -o  "$isabandoned" != "" ] ; then 
         git mv -k -v "${i}" Inactive/
    fi
done

printf "Number of tasks in Active/ after cleanup ---> %d\n" $(ls Active/*txt 2>/dev/null | wc -l)

git commit -m 'Archived inactive tasks.' 

popd &>/dev/null
