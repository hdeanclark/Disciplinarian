#!/usr/bin/env bash

thefile="$1"
basedir="$2"
thefile=$(realpath "$thefile")

pushd . &>/dev/null
cd "$basedir"
vim "$thefile"
popd &>/dev/null

( disciplinarian_update_uid_mapping.sh "$basedir" || true ) &

set -e

git -C "$basedir" add "$thefile" || true
git -C "$basedir" commit -m 'Open, edit, and commit script.' || true
wait


