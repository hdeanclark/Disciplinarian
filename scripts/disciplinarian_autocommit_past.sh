#!/usr/bin/env bash

thefile="$1"
basedir="$2"
thefile=$(realpath "$thefile")

set -e

git -C "$basedir" add "$thefile" || true
git -C "$basedir" commit -m 'Autocommit: inserted past time for task.' || true


